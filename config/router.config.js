export default [
  // admin
  {
    path: '/admin', component: '../layouts/UserLayout',
    routes: [
      { path: '/admin', redirect: '/admin/login' },
      { path: '/admin/login', name: 'login', component: './Admin/Login' },
      { path: '/admin/register', name: 'register', component: './Admin/Register' },
      { path: '/admin/register-result', name: 'register.result', component: './Admin/RegisterResult', },
      { component: '404' },
    ],
  },
  // user
  {
    path: '/user',component: '../layouts/UserLayout',
    routes: [
      { path: '/user', redirect: '/user/login' },
      { path: '/user/login', name: 'login', component: './User/Login' },
      { path: '/user/register', name: 'register', component: './User/Register' },
      { path: '/user/register-result', name: 'register.result', component: './User/RegisterResult', },
      { component: '404', },
    ],
  },
  // app
  {
    path: '/', component: '../layouts/BasicLayout', Routes: ['src/pages/Authorized'],
    routes: [
      { path: '/', redirect: '/course', authority: ['admin', 'user'] },
      // 教师
      { path: '/teacher', icon: 'user', name: 'teacher', component: './Teacher/Teacher' },
      // 学生
      { path: '/student', icon: 'team', name: 'student', component: './Student/Student' },
      { path: '/student/importing', icon: 'user', name: 'student', component: './Student/Importing', hideInMenu: true, },
      { path: '/student/importing/:id', icon: 'user', name: 'student', component: './Student/ImportingStudent', hideInMenu: true, },
      // 课程
      { 
        path: '/course', icon: 'schedule', name: 'course',
        routes: [
          { path: '/course/package', icon: 'read', name: 'package', component: './Course/Package/PackageList', },
          { path: '/course/group', icon: 'read', name: 'group', component: './Course/Group/GroupList', },
          { path: '/course/scheduling', icon: 'read', name: 'scheduling', component: './Course/CourseList', },
          { path: '/course/fenban', icon: 'read', name: 'fenban', component: './Course/FenbanList', },
          { path: '/course/package/:id', name: 'lesson', component: './Course/Package/PackageLesson', hideInMenu: true, },
          { path: '/course/package/:packageId/video/:lessonId', name: 'point', component: './Course/Package/PackageLessonPoint', hideInMenu: true, },
          { path: '/course/scheduling/:id', name: 'lesson', component: './Course/LessonList', hideInMenu: true, },
          { path: '/course/fenban/:id/student', name: 'fenbanstudent', hideInMenu: true, component: './Course/Fenban/StudentList',},
          { path: '/course/fenban/:id/teacher', name: 'fenbanteacher', hideInMenu: true, component: './Course/Fenban/TeacherList', },
        ]
      },
      // 订单
      { path: '/order', icon: 'money-collect', name: 'order', component: './Order/OrderList' },
      // 优惠券
      { path: '/discount', icon: 'dollar', name: 'discount', component: './DisCount/DisCount' },
      // 渠道
      { path: '/channel', icon: 'branches', name: 'channel', component: './Channel/ChannelList', },
      { path: '/channel/course/:channelId/:channelName', icon: 'pay-circle', name: 'channel.course', component: './Channel/channelCourse', hideInMenu: true, },
      { path: '/channel/activity/:channelId', icon: 'pay-circle', name: 'channel.activity', component: './Channel/ActivityList', hideInMenu: true, },
      // 渠道学员
      { path: '/channelStudent', icon: 'cluster', name: 'channelStudent', component: './ChannelStudents/ChannelMember', },
      // 活动
      {
        path: '/activity',icon: 'account-book', name: 'activity',
        routes: [
          { path: '/activity/grouppurchase', name: 'grouppurchase', component: './GroupPurchase/GroupPurchase' },
          { path: '/activity/freeevents', name: 'freeevents', component: './Activity/FreeList', },
        ]
      },
      // 赛事
      {
        path: '/events',icon: 'crown',name: 'events',
        routes: [
          { path: '/events/review', name: 'review', component: './Events/review' },
          { path: '/events/configuration', name: 'configuration', component: './Events/configuration', },
          { path: '/events/player', name: 'player', component: './Events/player', },
          { path: '/events/judge', name: 'judge', component: './Events/judge', },
        ]
      },
      // 反馈
      { path: '/feedback', icon: 'notification', name: 'feedback', component: './Feedback/FeedbackList', },
      // 举报
      { path: '/report', icon: 'thunderbolt', name: 'report', component: './Report/ReportList', },
      { component: '404', },
    ],
  },
];
