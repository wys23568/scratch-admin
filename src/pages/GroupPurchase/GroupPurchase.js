import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { Card, Form, Input, Select, Button, message, Divider, DatePicker, Modal } from 'antd';
import ServiceTable from '@/component/ServiceTable';
import GroupPurchaseModal from '@/component/ServiceGroupPurchase';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import router from 'umi/router';
import styles from './GroupPurchase.less';
import { debug } from 'util';
import { validateSearch } from 'rc-mentions/lib/util';

const FormItem = Form.Item;
const { Option } = Select;
const ButtonGroup = Button.Group;
@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
@Form.create()
class ReportList extends PureComponent {
  state = {
    searchPageList: {
      content: [],
      pageable: {},
    },
    activityModalVisible: false,
    modalObj: {},
    courseType: 0,
    courseList: [],
    courseGroupList: [],
    modalType: '',
    channelModalVisible: false,
    channelList: [],
    channelInfo: {},
    chooseChannelList: [],
  };
  componentDidMount() {
    this.beginSearch({ pageTotal: 0 });
    this.fetchCourseList();
    this.fetchCourseGroupList();
  }
  // 表格抬头
  columns = [
    {
      title: '活动Id',
      dataIndex: 'id',
      render: text => <span>{text}</span>,
    },
    {
      title: '活动名称',
      dataIndex: 'name',
      render: val => <span>{val}</span>,
    },
    {
      title: '团购价格',
      dataIndex: 'price',
      render: val => <span>{val}元</span>,
    },
    {
      title: '成团人数',
      dataIndex: 'restrictionCount',
      render: val => <span>{val}</span>,
    },
    {
      title: '课程ID',
      dataIndex: 'course',
      render: (index,val) => {
        return (
          <Fragment>
            {val.courseType == 1 && <span>{val.courseIds}</span>}
            {val.courseType == 2 && <span>{val.courseGroupIds}</span>}
          </Fragment>
        )
      },
    },
    {
      title: '活动截止时间',
      dataIndex: 'endAt',
      render: val => <span>{moment(val).format('YYYY-MM-DD HH:mm:ss')}</span>,
    },
    {
      title: '关联渠道',
      dataIndex: 'channelData',
      render: (val,record) => {
        let channelData = [];
        record.channelActivityInfoList.map(item => {
          channelData.push(item.channelId);
        })
        return (<Fragment>{channelData.join(",")}</Fragment>)
      },
    },
    {
      title: '状态',
      dataIndex: 'state',
      render: val => <span>{val == 0 ? "未上线" : "已上线"}</span>,
    },
    {
      title: '操作',
      render: (text, record) => {
          return (
            <Fragment>
              <a onClick={() => this.reWrite(record)}>修改</a>
              <Divider type="vertical" />
              <a onClick={() => this.openChannelModal(record)}>
                关联渠道
              </a>
              {/* <Divider type="vertical" />
              <a onClick={() => this.deleteActivity(record.id)}>
                删除活动
              </a> */}
              <Divider type="vertical" />
              {record.state == 0 && <a onClick={() => this.updateStatus(record.id,record.state)}>上线</a>}
              {record.state == 1 && <a onClick={() => this.updateStatus(record.id,record.state)}>下线</a>}
            </Fragment>
          );
      },
    },
  ];
  // 团购活动列表
  beginSearch = params => {
    const { dispatch } = this.props;
    params.searchState = this.searchState;
    this.searchParams = params;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `account/admin/activity/tuan/list`, data: params },
      callback: response => {
        if (response.result) {
          this.setState({
            searchPageList: response.data,
          });
        } else {
          this.setState({
            searchPageList: {},
          });
        }
      },
    });
  };
  // 新建
  handleActivityModal = (flag) => {
    console.log(this.state.courseType,this.state.modalObj)
    this.setState({
      activityModalVisible: true,
      modalType: 'add'
    })
  }
  // 活动设置 课程类型选择
  chooseCourseType = e => {
    this.setState({
      courseType: e
    })
  }
  // 新增活动 - 课程类型：单课程 -> 课程ID 列表
  fetchCourseList = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `course/online/buy`, data: {} },
      callback: response => {
        if (response.result) {
          let courseList = [];
          response.data.map(item => {
            courseList = courseList.concat(item.courseInfoList);
          })
          this.setState({
            courseList,
          });
        } else {
          this.setState({
            courseList: [],
          });
        }
      },
    });
  }
  // 新增活动 - 课程类型：系列课 -> 课程ID 列表
  fetchCourseGroupList = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `course/online/buy4Group`, data: {} },
      callback: response => {
        if (response.result) {
          let courseGroupList = [];
          // response.data.map(item => {
          //   courseGroupList = courseGroupList.concat(item.courseInfoList);
          // })
          this.setState({
            courseGroupList: response.data,
          });
        } else {
          this.setState({
            courseGroupList: [],
          });
        }
      },
    });
  }
  // 模态框确认
  setActivityOk = () => {
    const { form,dispatch } = this.props;
    const { modalType,modalObj } = this.state;
    form.validateFields((err,values) => {
      // return false;
      if(err) {
        return false;
      }
      
      values.startAt = values.startAt.format('YYYY-MM-DD HH:mm:ss')
      values.endAt = values.endAt.format('YYYY-MM-DD HH:mm:ss')
      if (modalType == "rewrite" && modalObj.state == 1) {
        console.log(values)
        // values.courseGroupIds = (modalObj.courseGroupIds.concat(values.courseGroupIds)).join(",");
        // values.courseIds = (modalObj.courseIds.concat(values.courseIds)).join(",");
        values.courseGroupIds && (values.courseGroupIds = (modalObj.courseGroupIds.concat(values.courseGroupIds)).join(","))
        values.courseIds && (values.courseIds = (modalObj.courseIds.concat(values.courseIds)).join(","));
      } else {
        values.courseGroupIds && (values.courseGroupIds = values.courseGroupIds.join(","))
        values.courseIds && (values.courseIds = values.courseIds.join(","))
      }
      modalType == "add" && this.submitCourseData(values);
      modalType == "rewrite" && this.reWriteCourseData(values);
    })
  }
  // 新增提交数据
  submitCourseData = params => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `account/admin/activity/tuan/add`, data: { ...params } },
      callback: response => {
        if (response.result) {
          message.success('添加成功');
          this.beginSearch({ pageTotal: 0 });
          this.handleModalCancel()
        } else {
          message.error('添加失败');
        }
      },
    });
  }
  // 修改 弹窗
  reWrite = argus => {
    let modalObj = {...argus};
    modalObj.courseIds = argus.courseIds && argus.courseIds!="" && argus.courseIds.split(",");
    modalObj.courseGroupIds = argus.courseGroupIds && argus.courseGroupIds!="" && argus.courseGroupIds.split(",");
    this.setState({
      courseType: argus.courseType,
      activityModalVisible: true,
      modalType: 'rewrite',
      modalObj,
    })
  }
  // 修改更新 活动数据
  reWriteCourseData = params => {
    const { dispatch } = this.props;
    params.activityId = this.state.modalObj.id;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `account/admin/activity/tuan/update`, data: { ...params } },
      callback: response => {
        if (response.result) {
          message.success('修改成功');
          this.beginSearch({ pageTotal: 0 });
          this.handleModalCancel()
        } else {
          message.error(response.message);
        }
      },
    });

  }
  // 模态框 取消
  handleModalCancel = () => {
    const { form } = this.props;
    this.setState({
      activityModalVisible: false,
      channelModalVisible: false,
      modalObj: {},
      courseType: 0,
      channelInfo: [],
      chooseChannelList: [],
    })
    form.resetFields();
  }
  // 删除活动
  deleteActivity = params => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `account/admin/activity/tuan/delete`, data: { activityId: params } },
      callback: response => {
        if (response.result) {
          message.success('删除成功');
          this.beginSearch({ pageTotal: 0 });
          this.setState({ activityModalVisible: false });
        } else {
          message.error('添加失败');
        }
      },
    });
  }
  // 打开关联渠道
  openChannelModal = record => {
    const {channelInfo} = this.state;
    this.setState({
      channelModalVisible: true,
      channelInfo: record,
    })
    if(record.channelActivityInfoList && record.channelActivityInfoList.length > 0) {
      let chooseChannelList = this.state.chooseChannelList;
      record.channelActivityInfoList.map(item => {
        chooseChannelList.push(item.channelId + "&" + item.channelName);
      })
      this.setState({
        chooseChannelList,
      })
    }
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `account/admin/channel/list`, data: { pageSize: -1 } },
      callback: response => {
        if (response.result) {
          this.setState({
            channelList: response.data.content
          })
        } else {
          message.error('添加失败');
        }
      },
    });
  }
  handleChildValueChange(event){
    // var value = event.target.value;
    console.log("元素",event)
    this.setState({
      chooseChannelList: event,
    })
  }
  // 更新状态
  updateStatus = (activityId,status) => {
    const state = status == 0 ? 1 : 0;
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `account/admin/activity/tuan/updateState`, data: { activityId,state } },
      callback: response => {
        if (response.result) {
          message.success('更新成功');
          this.beginSearch({ pageTotal: 0 });
        } else {
          message.error('添加失败');
        }
      },
    });
  }
  render() {
    const { loading, form: { getFieldDecorator } } = this.props;
    const { activityModalVisible, modalObj, searchPageList, courseType,modalType, courseList, courseGroupList, channelModalVisible,channelInfo, channelList,chooseChannelList  } = this.state;
    const formItemLayout = { labelCol: {xs: { span: 24 },sm: { span: 7 }}, wrapperCol: {xs: { span: 24 },sm: { span: 12 }} };
    const courseTypeList =[ { value: 1, text: '单课程' },{ value: 2, text: '系列课' } ]
    const formField = courseType == 1 ? "courseIds" : "courseGroupIds";
    return (
      <PageHeaderWrapper title="团购活动">
        <Card bordered={false}>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={() => this.handleActivityModal(true)}>
              新建
            </Button>
          </div>
          <div className={styles.tableList}>
            <ServiceTable
              selectedRows={[]}
              loading={loading}
              data={searchPageList}
              columns={this.columns}
              onChangeSearch={this.handleServiceTableSearchChange}
            />
          </div>
        </Card>
        <Modal
          title="活动设置"
          centered
          visible={activityModalVisible}
          onOk={this.setActivityOk}
          onCancel={this.handleModalCancel}
          bodyStyle={{ overflowY:'scroll'}}
        >
          <Form {...formItemLayout}>
            <FormItem label="活动名称">
              {getFieldDecorator('name', {
                initialValue: modalObj.name ? modalObj.name : "",
                rules: [{ required: true, message: '请输入活动名称' }],
              })(
                <Input />
              )}
            </FormItem>
            <FormItem label="课程类型">
              {getFieldDecorator('courseType', {
                initialValue: modalObj.courseType ? "" + modalObj.courseType : "",
                rules: [{ required: true, message: '请选择课程类型' }],
              })(
                <Select placeholder="请选择" onChange= {this.chooseCourseType}  disabled={modalType == "rewrite" && modalObj.state == 1}>
                  {
                    courseTypeList.map(item => {
                      return <Option key={item.value}>{item.text}</Option>
                    })
                  }
                </Select>
              )}
            </FormItem>
            {
              modalType == "rewrite" && modalObj.state == 1 && <FormItem label="已添加课程ID">
                <div>{courseType == 1 ? (modalObj.courseIds ? modalObj.courseIds.join(",") : "暂无添加") : (modalObj.courseGroupIds ? modalObj.courseGroupIds.join(",") : "暂无添加")}</div>
              </FormItem>
            }
            <FormItem label={modalType == "rewrite" && modalObj.state == 1  ? "增加课程" : "课程ID"}>
              {getFieldDecorator(formField, {
                initialValue: modalType == "rewrite" && modalObj.state == 0 ? (courseType == 1 ? (modalObj.courseIds ? modalObj.courseIds : []) : (modalObj.courseGroupIds ? modalObj.courseGroupIds : [])) : [],
                rules: [{ required: modalType == "rewrite" && modalObj.state == 1 ? false : true, message: '请选择课程ID' }],
              })(
                <Select
                  mode="multiple"
                  placeholder="请选择"
                  style={{ width: '100%' }}
                >
                  {courseType == 1 && courseList.map(item => {
                    return <Option key={item.id}>{item.id}</Option>
                  })}
                  {courseType == 2 && courseGroupList.map(item => {
                    return <Option key={item.id}>{item.id}</Option>
                  })}
                </Select>
              )}
            </FormItem>
            <FormItem label="活动开始时间">
              {getFieldDecorator("startAt", {
                initialValue: modalObj.startAt ? moment(modalObj.startAt) : undefined,
                rules: [ { required: true,message: `请选择活动开始时间` } ],
              })(
                <DatePicker
                  showTime={{ format: 'HH:mm:ss' }}
                  format="YYYY-MM-DD HH:mm:ss"
                  placeholder="请输入"
                  style={{width: '100%'}}
                />
              )}
            </FormItem>
            <FormItem label="活动结束时间">
              {getFieldDecorator("endAt", {
                initialValue: modalObj.endAt ? moment(modalObj.endAt) : undefined,
                rules: [ { required: true, message: `请选择活动结束时间` } ],
              })(
                <DatePicker
                  showTime={{ format: 'HH:mm:ss' }}
                  format="YYYY-MM-DD HH:mm:ss"
                  placeholder="请输入"
                  style={{width: '100%'}}
                />
              )}
            </FormItem>
            <FormItem label="团购价格">
              {getFieldDecorator('price', {
                initialValue: modalObj.price ? modalObj.price : "",
                rules: [{ required: true, message: '请输入团购价格' }],
              })(
                <Input />
              )}
            </FormItem>
            <FormItem label="拼团人数">
              {getFieldDecorator('restrictionCount', {
                initialValue: modalObj.restrictionCount ? modalObj.restrictionCount : "",
                rules: [{ required: true, message: '请输入拼团人数' }],
              })(
                <Input disabled={modalType == "rewrite" && modalObj.state == 1}/>
              )}
            </FormItem>
          </Form>
        </Modal>
        <GroupPurchaseModal
          modalVisible={channelModalVisible}
          refreshTable={this.beginSearch}
          handleCancel={this.handleModalCancel}
          channelList={channelList}
          channelInfo={channelInfo}
          chooseChannelList={chooseChannelList}
          handleChildValueChange={this.handleChildValueChange.bind(this)}
        />
      </PageHeaderWrapper>
    );
  }
}

export default ReportList;