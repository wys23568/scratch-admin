import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { Row, Card, Form, Input,InputNumber, Select, Radio, Button, message, Modal, Divider,Popconfirm } from 'antd';
import ServiceTable from '@/component/ServiceTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './DisCount.less';
const FormItem = Form.Item;
const { Option } = Select;

@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
@Form.create()
class ReportList extends PureComponent {
  state = {
    modalVisible: false,
    modalType: '',
    showForm: false,
    disCountType: '',
    dictionary: [],
    rowCouponInfo: {},
    selectedRows: [],
    searchPageList: {
      content: [],
      pageable: {},
    },
  };
  // 表格抬头
  columns = [
    {
      title: '优惠券Id',
      dataIndex: 'id',
      render: text => <span>{text}</span>,
    },
    {
      title: '优惠券种类',
      dataIndex: 'category',
      render: val => <span>{val==0 ? '通用' : 'scratch'}</span>,
    },
    {
      title: '优惠券类型',
      dataIndex: 'type',
      render: val => <span>{val==0 ? '抵用券' : '折扣券'}</span>,
    },
    {
      title: '名称',
      dataIndex: 'name',
      render: val => <span>{val}</span>,
    },
    {
      title: '优惠额度/折扣',
      dataIndex: 'amount',
      render: (val,rowVal) => {
        return (
          <Fragment>
            {rowVal.type == 0 ? <span>{val}元</span> : <span>{ val }折</span>}
          </Fragment>
        )
      },
    },
    {
      title: '满减额度',
      dataIndex: 'money',
      render: val => <span>{val}</span>,
    },
    {
      title: '有效期',
      dataIndex: 'validityDays',
      render: val => <span>{val}</span>,
    },
    {
      title: '修改日期',
      dataIndex: 'updatedAt',
      render: val => <span>{moment(val).format('YYYY-MM-DD HH:mm:ss')}</span>,
    },
    {
      title: '状态',
      dataIndex: 'state',
      render: val => <span>{ val == 0 ? '未发放' : '已发放' }</span>,
    },
    {
      title: '操作',
      render: (val) => {
          return (
            <Fragment>
              {val.state == 0 ? <Popconfirm title="确定删除此优惠券?" onConfirm={() => this.deleteCoupon(val.id)}><a>删除</a></Popconfirm> : ''}
              <Divider type="vertical" />
              {val.state == 0 ? <a onClick={() => this.reWriteCoupon(val)}>修改</a> : <span>不可修改</span>}
            </Fragment>
          );
      },
    },
  ];
  componentDidMount() {
    this.beginSearch({ pageTotal: 0,pageSize: 10 });
    this.fetchDict();
  }
  // 初始化表格数据
  beginSearch = params => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `account/admin/discount/list`, data: params },
      callback: response => {
        if (response.result) {
          this.setState({
            searchPageList: response.data,
          });
        } else {
          this.setState({
            searchPageList: {},
          });
        }
      },
    });
  };
  // 表格改变
  handleTableSearch = (pagination, filtersArg, sorter) => {
    const { formValues } = this.state;
    const filtersArray = [];
    const filtersKeys = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = '' + key + ':' + getValue(filtersArg[key], ',');
      filtersArray.push(newObj[key]);
      return newObj;
    }, {});
    const filters = filtersArray.join(';');
    const params = {
      pageNumber: pagination.current - 1,
      pageSize: pagination.pageSize,
      pageTotal: pagination.total,
      ...formValues,
      filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }
    this.beginSearch(params);
  };
  handleSelectRows = rows => {
    const { onSelect } = this.props;
    this.setState({
      selectedRows: rows,
    });
    if (onSelect) {
      onSelect(rows);
    }
  };
  // 新建 - 弹窗
  handleModalCreate = (flag,type) => {
    const { dispatch,form } = this.props;
    form.resetFields();
    this.setState({
      modalVisible: flag,
      modalType: type,
      disCountType: flag ? this.state.disCountType : '',
      rowCouponInfo: flag ? this.state.rowCouponInfo : {},
      showForm: flag? this.state.showForm : false,
    });
  };
  // 请求优惠券种类
  fetchDict = () => {
    const { dispatch,form } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `config/dict`, data: {type: 'DISCOUNT_CATEGORY'} },
      callback: response => {
        if (response.result) {
          this.setState({
            dictionary: response.data,
          });
        } else {
          this.setState({
            dictionary: [],
          });
        }
      },
    });
  }
  // 修改信息
  reWriteCoupon = (values) => {
    this.handleModalCreate(true,'reWrite')
    this.setState({
      rowCouponInfo: values,
      modalType: 'reWrite',
      showForm: true,
      disCountType: values.type
    })
  }
  // 选择优惠券类型
  radioChange = (e) => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      showForm: true,
      disCountType: Number(e.target.value)
    })
  }
  // 新建优惠券 - 处理数据
  createCoupon = () => {
    const { form } = this.props;
    const { disCountType,modalType } = this.state;
    form.validateFields((err,values) => {
      if(err) {
        return false;
      }
      console.log(values)
      if(modalType == "add") {
        this.pushCoupon(values);
      } else if (modalType == "reWrite") {
        this.updateCoupon(values);
      }
    })
  }
  // 新建优惠券 - 发送请求
  pushCoupon = (values) => {
    const { dispatch,form } = this.props;
    const params = {
      ...values,
      courseType: 2,
    }
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `account/admin/discount/add`, data: params },
      callback: res => {
        if(res.result) {
          message.success('优惠券创建成功');
          this.beginSearch({ pageTotal: 0,pageSize: 10 });
          this.handleModalCreate(false,'no');
        } else {
          message.error(res.error);
        }
      },
    });
  }
  // 更新优惠券 - 发送请求
  updateCoupon = (values) => {
    const { dispatch,form } = this.props;
    const { rowCouponInfo } = this.state;
    const params = {
      ...values,
      courseType: 2,
      discountId: rowCouponInfo.id
    }
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `account/admin/discount/update`, data: params },
      callback: res => {
        if(res.result) {
          message.success('优惠券信息修改成功');
          this.beginSearch({ pageTotal: 0,pageSize: 10 });
          this.handleModalCreate(false,'no');
        } else {
          message.error(res.error);
        }
      },
    });
  }
  // 删除优惠券
  deleteCoupon = (discountId) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `account/admin/discount/delete`, data: {discountId} },
      callback: res => {
        if(res.result) {
          message.success('当前优惠券已删除');
          this.beginSearch({ pageTotal: 0,pageSize: 10 });
        } else {
          message.error(res.error);
        }
      },
    });
  }
  // 公用表单
  commonForm = () => {
    const { form: { getFieldDecorator} } = this.props;
    const { dictionary, rowCouponInfo } = this.state;
    return (
      <Fragment>
        <Row>
          <FormItem label="优惠券种类:" >
            {getFieldDecorator('category', {
              initialValue: rowCouponInfo.category ? "" + rowCouponInfo.category :  (rowCouponInfo.category == 0 ? "0" : ""),
              // initialValue: rowCouponInfo.category ? (rowCouponInfo.category == 0 ? 0 : 1) : "",
              rules: [{ required: true, message: '请选择优惠券种类' }],
            })(
            <Select
              placeholder="请选择"
            >
              {dictionary.map(item => <Option key={item.value} value={item.value}>{item.name}</Option>)}
            </Select>
            )}
          </FormItem>
        </Row>
        <Row>
          <FormItem label="名称:">
            {getFieldDecorator('name', {
              initialValue: rowCouponInfo.name ?  rowCouponInfo.name : "",
              rules: [{ required: true, message: '请输入名称' }],
            })(
              <Input />
            )}
          </FormItem>
        </Row>
      </Fragment>
    )
  }
  // 折扣券表单
  formCoupon = () => {
    const { form: { getFieldDecorator} } = this.props;
    const { rowCouponInfo } = this.state;
    return (
      <Fragment>
        <Row>
          <FormItem label="优惠折扣(折):">
            {getFieldDecorator('amount', {
              initialValue: rowCouponInfo.amount ?  rowCouponInfo.amount : "",
              rules: [{ required: true, message: '请输入折扣' }],
            })(
              <InputNumber style={{width: '100%'}} min={0}/> 
            )} 
          </FormItem>
        </Row>
        <Row>
          <FormItem label="满减额度(元):" help=" 大于该值时可用">
            {getFieldDecorator('money', {
              initialValue: rowCouponInfo.money ?  rowCouponInfo.money : "",
              rules: [{ required: true, message: '请输入满减额度' }],
            })(
              <InputNumber style={{width: '100%'}} min={0}/>
            )}
          </FormItem> 
        </Row>
        <Row>
          <FormItem label="有效期(天):">
            {getFieldDecorator('validityDays', {
              initialValue: rowCouponInfo.validityDays ?  rowCouponInfo.validityDays : "",
              rules: [{ required: true, message: '请输入有效期' }],
            })(
              <InputNumber style={{width: '100%'}} min={0}/>
            )}  
          </FormItem> 
        </Row>
      </Fragment>
    )
  }
  // 抵用券表单
  formVvouchers = () => {
    const { form: { getFieldDecorator} } = this.props;
    const { rowCouponInfo } = this.state;
    return (
      <Fragment>
        <Row>
          <FormItem label="优惠额度(元):">
            {getFieldDecorator('amount', {
              initialValue: rowCouponInfo.amount ?  rowCouponInfo.amount : "",
              rules: [{ required: true, message: '请输入优惠额度' }],
            })(
              <InputNumber style={{width: '100%'}} min={1}/> 
            )} 
          </FormItem>
        </Row>
        <Row>
          <FormItem label="满减额度(元):" help=" 大于该值时可用">
            {getFieldDecorator('money', {
              initialValue: rowCouponInfo.money ?  rowCouponInfo.money : "",
              rules: [{ required: true, message: '请输入满减额度' }],
            })(
              <InputNumber style={{width: '100%'}} min={1}/>
            )}
          </FormItem> 
        </Row>
        <Row>
          <FormItem label="有效期(天):">
            {getFieldDecorator('validityDays', {
              initialValue: rowCouponInfo.validityDays ?  rowCouponInfo.validityDays : "",
              rules: [{ required: true, message: '请输入有效期' }],
            })(
              <InputNumber style={{width: '100%'}} min={1}/>
            )}  
          </FormItem> 
        </Row>
      </Fragment>
    )
  }
  render() {
    const { loading,form: { getFieldDecorator} } = this.props;
    const { modalVisible, searchPageList, selectedRows, showForm, disCountType, rowCouponInfo } = this.state;
    const formItemLayout = { labelCol: {xs: { span: 24 },sm: { span: 7 }}, wrapperCol: {xs: { span: 24 },sm: { span: 12 }} };
    return (
      <PageHeaderWrapper title="优惠券">
        <Card bordered={false}>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={() => this.handleModalCreate(true,'add')}>
              新建
            </Button>
          </div>
          <div className={styles.tableList}>
            <ServiceTable
              rowKey="id"
              selectedRows={selectedRows}
              loading={loading}
              data={searchPageList}
              columns={this.columns}
              onChangeSearch={this.handleTableSearch}
            />
          </div>
        </Card>
        <Modal
          title="优惠券新建"
          centered
          visible={modalVisible}
          onOk={this.createCoupon}
          okButtonProps={{disabled: !showForm}}
          onCancel={() => this.handleModalCreate(false,'no')}
          bodyStyle={{height: '50vh', overflowY:'scroll'}}
        >
          <Form {...formItemLayout} style={{height: '65vh'}}>
            <Row>
              <FormItem label="选择优惠券类型">
                {getFieldDecorator('type', {
                  initialValue: "" + rowCouponInfo.type,
                  rules: [{ required: true, message: '选择优惠券类型' }],
                })(
                  <Radio.Group onChange={this.radioChange}>
                    <Radio value="0">抵用券</Radio>
                    <Radio value="1">折扣券{showForm}</Radio>
                  </Radio.Group>
                )}
              </FormItem>
            </Row>
            {showForm}
            {showForm ? this.commonForm() : ""}
            {showForm ? (disCountType === 0 ? this.formVvouchers() : this.formCoupon()) : ""}
          </Form>
          
        </Modal>
      </PageHeaderWrapper>
    );
  }
}

export default ReportList;
