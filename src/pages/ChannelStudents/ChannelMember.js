import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { Row, Col, Card, Form, Input, Button, message, Badge,Select,Upload,Icon } from 'antd';
import ServiceTable from '@/component/ServiceTable';
import ServiceModal from '@/component/ServiceModal';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './ChannelMember.less';

const ButtonGroup = Button.Group;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const stateFilters = [
  { value: 0, text: '创建成功', icon: 'success', enable: true },
  { value: 1, text: '已存在', icon: 'warning', enable: true },
  { value: 2, text: '失败', icon: 'error', enable: true },
];

function hasErrors (fieldsError) {
  return Object.keys(fieldsError).some(field => fieldsError[field]);
}
@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))

@Form.create()
class ChannelMember extends PureComponent {
  state = {
    packageList: [],
    fileName: '',
    fileList: {
      content: [],
      pageable: {},
    },
    modalObj: {},
    modalVisible: false,  
    channelList: [],  // 渠道数据
    courseList: [],   // 课程数据
    curCourse: {},    // 当前选择课程数据
    discountInfoList: [], // 优惠券列表
    discountInfoString: '', // 优惠券列表字符串
    discountInfoId: '', // 优惠券列表字id集合字符串
    
  };
  modalUrl = 'account/admin/student/order';
  modalDef = [
    {
      title: '姓名',
      name: 'name',
      type: 'string',
    },
    {
      title: '手机号',
      name: 'phone',
      type: 'string',
    },
  ];
  // 表格title
  columns = [
    {
      title: '序号',
      dataIndex: 'number',
      render: text => <span>{text}</span>,
      width: '5%'
    },
    {
      title: '姓名',
      dataIndex: 'name',
      render: text => <span>{text}</span>,
      width: '8%'
    },
    {
      title: '手机号',
      dataIndex: 'phone',
      render: val => <span>{val}</span>,
      width: '10%'
    },
    {
      title: '状态',
      dataIndex: 'resultType',
      render(val) {
        return stateFilters[val] ? (
          <Badge status={stateFilters[val].icon} text={stateFilters[val].text} />
        ) : (
          ''
        );
      },
    },
  ];
  componentDidMount() {
    this.props.form.validateFields();
    this.fetchChannelData({ pageSize: -1,state: 1  });
  }
  // 模板下载
  downLoadFile = () => {
    window.location.href="https://haimacode.oss-cn-beijing.aliyuncs.com/storage/4038/5661820/05df96f304690d3e3bf0c933d883a8b8.xlsx";
  }
  // 上传文件
  handleFile = (info) => {
    this.setState({
      fileName: info.file.name,
    })
    const { status,response={} } = info.file;
    if( status === 'done') {
      if(response.code != 200) {
        message.error(response.error);
        return false;
      }
      this.getPhones(info.file.response.data);
    }
  }
  // 获取发放表单手机号字段
  getPhones = (addArgus) => {
    const { form } = this.props;
    let phonesArr = [];

    const { fileList } = this.state;
    let objHa  = {
      content: addArgus.concat(fileList.content),
      pageable: {},
    }
    objHa.content.map((curV,index) => {
      curV.number = index +1;
      // if (curV.resultType == 0) {  // 校验手机号是否已存在
        phonesArr.push(curV.phone)
      // }
    })
    this.setState({
      fileList: objHa,
    })
    if(phonesArr.length > 0) {
      form.setFieldsValue({'phones':phonesArr.join(",")});
    }
  }
  // 手动添加
  handleModalCreate = () => {
    this.setState({
      modalVisible: true,
    });
  };
  // 查询渠道
  fetchChannelData = params => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `account/admin/channel/list`, data: params },
      callback: response => {
        if (response.result) {
          this.setState({ channelList: [...response.data.content] });
        } else {
          this.setState({ channelList: [] });
        }
      },
    });
  };
  // 选择渠道
  changeChannel = argus => {
    this.setState({
      discountInfoString: '',
    })
    const { form,dispatch } = this.props;
    form.setFieldsValue({'courseInfo':' '});
    form.setFieldsValue({'discountId':' '});
    this.setState({
      discountInfoList: [],
    })
    const params = {
      channelId: argus,
      courseType: 1,
      pageSize: -1,
      pageNumber: 0,
    }
    this.fetchCourseData(params);
  }
  // 查询课程,获取课程/代金券数据
  fetchCourseData = params => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: {
        url: 'account/admin/channel/course/list',
        data: params
      },
      callback: response => {
        if (response.result) {
          this.setState({ courseList: [...response.data.content] });
        } else {
          this.setState({ courseList: [] });
        }
      }
    })
  }
  // 选择课程，获取代金券数据
  changeCourse = argus => {
    console.log(argus)
    const sIndex = argus.props.value.slice(0,1);
    const { courseList } = this.state;
    const { form } = this.props;
    this.setState({
      curCourse: courseList[sIndex],
      discountInfoList: courseList[sIndex].discountInfoList ? courseList[sIndex].discountInfoList : [],
    })
    let nameArr = [];
    courseList[sIndex].discountInfoList ? courseList[sIndex].discountInfoList.map((item,index) => {
      nameArr.push(item.name);
    }) : nameArr.push("暂无代金券")
    this.setState({
      discountInfoString: nameArr.join(" / "),
    })
    // form.setFieldsValue({'discountId':idArr.join(",")});
    form.setFieldsValue({'discountId':courseList[sIndex].discountIds});
  }
  // 刷新表格
  handleServiceTableSearchChange = (pagination, filtersArg, sorter) => {
  };
  // 确定发放代金券
  handleSubmit = e => {
    e.preventDefault();
    const { form,dispatch } = this.props;
    const { curCourse } = this.state;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue,
        courseType: curCourse.courseType,
        courseId: curCourse.courseId,
        courseGroupId: curCourse.courseGroupId,
      };
      dispatch({
        type: 'service/haimawangApiCall',
        payload: { url: 'account/admin/student/course/create', data: { ...values } },
        callback: res => {
          console.log(res)
          if(res.result) {
            message.success("发放成功");
          } else {
            message.warning(res.message);
          }
        }
      });
    });
  };
  // 手动添加模态框确定
  handleModalOk = obj => {
    const {  fieldsValue } = obj;
    const { dispatch } = this.props;
    var params = {
      name: fieldsValue.name,
      phone: fieldsValue.phone.replace(/\s+/g,"")
    }
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `account/admin/student/add`, data: { ...params } },
      callback: response => {
        if (response.result) {
          if(response.data.resultType == 0 || response.data.resultType == 1) {
            this.getPhones([response.data]);
            this.handleModalVisible(false);
            message.success(response.data.resultMsg);
          } 
          // else if (response.data.resultType == 1) {
          //   message.warning(response.data.resultMsg);
          // }
          else {
            message.error(response.data.resultMsg);
          }
        } else {
          message.error('添加失败');
        }
      },
      });
  };
  // 模态框取消
  handleModalCancel = () => {
    this.handleModalVisible(false);
  };
  // 模态框关闭
  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };
  // 上传文件组件
  renderUpload (){
    const { fileName,file } = this.state;
    let type = 0;
    if (window.location.href.indexOf('localhost') > 0) type = 1;
    if (window.location.href.startsWith('https://www.haimacode.com/test') || window.location.href.startsWith('https://www.haimacode.cn/test')) type = 1;
    if (window.location.href.startsWith('https://www.haimacode.com/admin') || window.location.href.startsWith('https://www.haimacode.cn/admin')) type = 2;
  
    let adminurl = `https://www.haimacode.com/api/haimawang/account/`;
    if (type === 2) adminurl = `https://www.haimacode.com/api/haimawang/account/`;
    if (type === 1) adminurl = `https://www.haimacode.com/test/api/haimawang/account/`;
    if (type === 0) adminurl = `http://localhost:8180/haimawang/account/`;
    const uploadProps = {
      action: `${adminurl}admin/student/import?loginAppId=20111113&loginAppKey=ab731a584eb3826cf1sdf6ee654c2d32`,
      showUploadList: false,
      onChange: this.handleFile,
      name: 'upload',
      accept: '.xls,.xlsx',
      data: {
        loginToken: localStorage.getItem('loginToken')
      }
    }
    return (
      <div className={styles.uploadBox}>
        <Upload {...uploadProps} className={styles.uploadLisInline}>
          <Button type="primary" htmlType="submit">
            <Icon type="upload" />
            上传文件
          </Button>
        </Upload>
        <Button style={{ marginLeft: 8 }} onClick={() => this.downLoadFile()}>
          下载模板
        </Button>
        <Button style={{ marginLeft: 8 }} onClick={() => this.handleModalCreate()}>
          手动添加
        </Button>
        <div className={styles.fileName}>{fileName}</div>
      </div>
    )
  }
  // 查询表单
  renderSimpleForm() {
    const { form: { getFieldDecorator,getFieldsError,getFieldError,isFieldTouched,getFieldValue} } = this.props;
    const { packageList,channelList,courseList,discountInfoList,phones,discountInfoString } = this.state;
    const channelIdError = isFieldTouched('channelId') && getFieldError('channelId');
    const courseInfoError = isFieldTouched('courseInfo') && getFieldError('courseInfo');
    const discountIdError = isFieldTouched('discountId') && getFieldError('discountId');
    const phonesError = isFieldTouched('phones') && getFieldError('phones');
    return (
      <Form layout="inline" onSubmit={this.handleSubmit} >
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={6} sm={24}>
            <FormItem label="渠道" validateStatus={channelIdError ? 'error' : ''} help={channelIdError || ''}>
              {getFieldDecorator('channelId', {
                rules: [{ required: true, message: '请选择渠道' }],
              })(
                <Select placeholder="请选择" style={{ width: '100%' }} onSelect={value => this.changeChannel(value)}>
                  {channelList.map(item => (
                    <Option key={item.id} >{item.name}</Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="课程" validateStatus={courseInfoError ? 'error' : ''} help={courseInfoError || ''}>
              {getFieldDecorator('courseInfo', {
                rules: [{ required: true, message: '请选择课程'}],
              })(
                <Select showSearch placeholder="请选择" style={{ width: '100%' }} onSelect={(index,key) => this.changeCourse(key)}>
                  {courseList.map((item,index) => (
                    <Option key={item.courseId} value={index + " " + item.courseIssue + " " +item.courseName}>{item.courseIssue} {item.courseName}</Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col md={6} sm={24}>
            <FormItem label="发放代金券"  validateStatus={discountIdError ? 'error' : ''} help={discountIdError || ''}>
              <span>{discountInfoString}</span>
              {getFieldDecorator('discountId', {
                rules: [{ required: false, message: '请选择代金券' }],
              })(
                <Input style={{opacity:'0',position: 'absolute'}} disabled/>
              )}
            </FormItem>
          </Col>
          <Col md={1} sm={24} style={{opacity: '0'}}>
            <FormItem label="手机号"  validateStatus={phonesError ? 'error' : ''} help={phonesError || ''}>
              {getFieldDecorator('phones', {
                rules: [{ required: true, message: '请上传合格手机号' }],
              })(
                <Input disabled/>
              )}
            </FormItem>
          </Col>
        </Row>
        <Row>
          <Col md={3} sm={24}>
            <span className={styles.submitButtons}>
              <Button type="primary" htmlType="submit" disabled={hasErrors(getFieldsError())}>
                确定
              </Button>
            </span>
          </Col>
        </Row>
      </Form>
    );
  }
  renderForm() {
    return this.renderSimpleForm();
  }
  render() {
    const { loading } = this.props;
    const { fileList,modalVisible,modalObj } = this.state;
    return (
      <PageHeaderWrapper title="渠道学员管理">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div>{this.renderUpload()}</div>
            <ServiceTable
              selectedRows={[]}
              loading={loading}
              data={fileList}
              columns={this.columns}
              onChangeSearch={this.handleServiceTableSearchChange}
            />
            <div className={styles.tableListForm}>{this.renderForm()}</div>
          </div>
        </Card>
        <ServiceModal
          title="课程"
          handleOK={this.handleModalOk}
          handleCancel={this.handleModalCancel}
          modalObj={modalObj}
          modalDef={this.modalDef}
          modalVisible={modalVisible}
        />
      </PageHeaderWrapper>
    );
  }
}

export default ChannelMember;