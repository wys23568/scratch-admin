import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import router from 'umi/router';
import { Input, Row, Col, Card, Form, Select, Icon, Button, Dropdown, Menu, message, Badge, Divider, Modal} from 'antd';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import ServiceTable from '@/component/ServiceTable';
import ServiceButton from '@/component/ServiceButton';

import styles from './FreeList.less';

const ButtonGroup = Button.Group;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = (obj, char) =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(char);
const stateFilters = [
  { value: 0, text: '禁用', icon: 'default' },
  { value: 1, text: '下线', icon: 'error' },
  { value: 2, text: '上线', icon: 'success' },
];

const courseTypeFilters = [
  { value: 0, text: '禁用', icon: 'error' },
  { value: 1, text: '单课', icon: 'success' },
  { value: 2, text: '年课', icon: 'success' },
];
@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
@Form.create()
class FreeList extends PureComponent {
  state = {
    modalVisible: false,
    formValues: {},
    searchPageList: {
      content: [],
      pageable: {},
    },
    modalVisible: false,
    modalObj: {},
    courseType: 0,
    courseList: [],
    courseGroupList: [],
    modalType: 0,
  };
  searchParams = {};
  searchState = -1;
  modalUrl = 'account/admin/activity/free';
  columns = [
    { title: '活动ID', dataIndex: 'id',width: '10em' },
    { title: '活动名称', dataIndex: 'name' },
    { title: '课程ID', dataIndex: 'course',width: '10em',
      render: (value,record) => {
        return record.courseType == 1 ? record.courseId : record.courseGroupId
      }
    },
    { title: '人数限制', dataIndex: 'quotaCount',width: '10em' },
    { title: '已领取人数', dataIndex: 'gainCount',width: '10em' },
    { title: '查看人数', dataIndex: 'lookCount',width: '10em' },
    {
      title: '状态',
      dataIndex: 'state',
      width: '10em',
      render: (value, record) => {
        return stateFilters[value] ? (
          <Badge status={stateFilters[value].icon} text={stateFilters[value].text} />
        ) : (
          ''
        );
      },
    },
    {
      title: '操作',
      render: (value, record) => (
        <Fragment>
          <a onClick={() => this.handleModalUpdate(record)}>修改</a>
          <Divider type="vertical" />
          {record.state !== 2 ? (
            <a onClick={() => this.handleStateUpdate(record.id, 2)}>上线</a>
          ) : (
            <a onClick={() => this.handleStateUpdate(record.id, 1)}>下线</a>
          )}
        </Fragment>
      ),
    },
  ];
  componentDidMount() {
    this.beginSearch({ pageTotal: 0 });
    this.fetchCourseList();
    this.fetchCourseGroupList();
  }
  // 初试表格数据查询
  beginSearch = searchParams => {
    const { dispatch, match } = this.props;
    this.searchParams = searchParams;
    this.searchParams.searchState = this.searchState;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/search`, data: this.searchParams },
      callback: response => {
        if (response.result) {
          this.setState({
            searchPageList: response.data,
          });
        } else {
          this.setState({
            searchPageList: {},
          });
        }
      },
    });
  };
  // 操作 - 修改
  handleModalUpdate = obj => {
    // if(obj.courseType == 1) {
    //   this.fetchCourseList()
    // } else if (obj.courseType == 2) {
    //   this.fetchCourseGroupList();
    // }
    this.setState({
      modalType: 2,
      modalVisible: true,
      modalObj: obj,
      courseType: obj.courseType,
    });
  };
  // 操作 - 上下线
  handleStateUpdate = (id, state) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `${this.modalUrl}/updateState`, data: { activityId: id, state: state } },
      callback: response => {
        if (response.result) {
          message.success('设置成功');
          this.beginSearch(this.searchParams);
        } else {
          message.error('设置失败');
        }
      },
    });
  };
  // 数据状态筛选
  handleStateSearch = state => {
    this.searchState = state;
    this.searchParams.pageTotal = 0;
    this.beginSearch(this.searchParams);
  };
  handleModalOk = obj => {
    const { modalObj, fieldsValue } = obj;
    const { dispatch, match } = this.props;
    const params = { ...fieldsValue };
    if (modalObj.id === undefined) {
      dispatch({
        type: 'service/haimawangApiCall',
        payload: { url: `${this.modalUrl}/add`, data: { ...params } },
        callback: response => {
          if (response.result) {
            message.success('添加成功');
            this.beginSearch(this.searchParams);
            this.setState({ modalVisible: false });
          } else {
            message.error('添加失败');
          }
        },
      });
    } else {
      dispatch({
        type: 'service/adminApiCall',
        payload: {
          url: `${this.modalUrl}/this/update`,
          data: { activityFreeId: modalObj.id, ...params },
        },
        callback: response => {
          if (response.result) {
            message.success('修改成功');
            this.beginSearch(this.searchParams);
            this.setState({ modalVisible: false });
          } else {
            message.error('修改失败');
          }
        },
      });
    }
  };
  handleTableSearch = (pagination, filtersArg, sorter) => {
    const { formValues } = this.state;
    const filtersArray = [];
    const filtersKeys = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = '' + key + ':' + getValue(filtersArg[key], ',');
      filtersArray.push(newObj[key]);
      return newObj;
    }, {});
    const filters = filtersArray.join(';');
    const params = {
      pageNumber: pagination.current - 1,
      pageSize: pagination.pageSize,
      pageTotal: pagination.total,
      ...formValues,
      filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }
    this.beginSearch(params);
  };
  // 弹窗控制
  handleModalCreate = flag => {
    const {form} = this.props;
    this.setState({
      modalVisible: flag,
      modalObj: {},
      modalType: flag ? 1 : 0,
    });
    !flag && form.resetFields();
  };
  // 新增活动 课程类型选择
  chooseCourseType = e => {
    this.setState({
      courseType: e
    })
  }
  // 新增活动 - 课程类型：单课程 -> 课程ID 列表
  fetchCourseList = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `course/online/free`, data: {} },
      callback: response => {
        if (response.result) {
          let courseList = [];
          response.data.map(item => {
            courseList = courseList.concat(item.courseInfoList);
          })
          this.setState({
            courseList,
          });
        } else {
          this.setState({
            courseList: [],
          });
        }
      },
    });
  }
  // 新增活动 - 课程类型：系列课 -> 课程ID 列表
  fetchCourseGroupList = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `course/online/buy4Group`, data: {} },
      callback: response => {
        if (response.result) {
          let courseGroupList = [];
          this.setState({
            courseGroupList:response.data,
          });
        } else {
          this.setState({
            courseGroupList: [],
          });
        }
      },
    });
  }
  // 提交数据
  submitData = () => {
    const { form } = this.props;
    form.validateFields((err,values) => {
      if(err) {
        return false;
      }
      if(this.state.modalType == 1) {
        this.submitCourseData(values);
      } else if(this.state.modalType == 2) {
        this.rewriteCourseData(values);
      }
    })
  }
  submitCourseData = params => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `${this.modalUrl}/add`, data: { ...params } },
      callback: response => {
        if (response.result) {
          message.success('添加成功');
          this.beginSearch(this.searchParams);
          this.setState({ modalVisible: false });
        } else {
          message.error('添加失败');
        }
      },
    });
  }
  rewriteCourseData = argus => {
    const { dispatch } = this.props;
    const { modalObj } = this.state;
    const params = {
      ...argus,
      activityId: modalObj.id
    }
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `${this.modalUrl}/update`, data: { ...params } },
      callback: response => {
        if (response.result) {
          message.success('修改成功');
          this.beginSearch(this.searchParams);
          this.setState({ modalVisible: false });
          this.handleModalCreate(false)
        } else {
          message.error('修改失败');
        }
      },
    });
  }
  render() {
    const { loading, form: { getFieldDecorator } } = this.props;
    const { modalVisible, modalObj, searchPageList, courseType, courseList, courseGroupList, modalType } = this.state;
    const formItemLayout = {
      labelCol: { xs: { span: 24 }, sm: { span: 6 } },
      wrapperCol: { xs: { span: 24 }, sm: { span: 14 } },
    };
    const courseTypeList =[ { value: 1, text: '单课程' },{ value: 2, text: '系列课' } ]
    const formField = courseType == 1 ? "courseId" : "courseGroupId";
    return (
      <PageHeaderWrapper title="免费活动">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalCreate(true)}>
                新建
              </Button>
              <ServiceButton
                stateFilters={stateFilters}
                searchState={this.searchState}
                handleStateSearch={state => this.handleStateSearch(state)}
              />
            </div>
            <ServiceTable
              selectedRows={[]}
              loading={loading}
              data={searchPageList}
              columns={this.columns}
              onChangeSearch={this.handleTableSearch}
            />
          </div>
        </Card>
        <Modal
          title={modalType == 2 ? "修改免费活动" : "新增免费活动"}
          centered
          visible={modalVisible}
          onOk={this.submitData}
          onCancel={() => this.handleModalCreate(false)}
        >
          <Form {...formItemLayout}>
              <FormItem label="活动名称">
                {getFieldDecorator('name', {
                  initialValue: modalObj.name ? modalObj.name : "",
                  rules: [{ required: true, message: '请输入活动名称' }],
                })(
                  <Input />
                )}
              </FormItem>
              <FormItem label="课程类型">
                {getFieldDecorator('courseType', {
                  initialValue: modalObj.courseType ? (modalObj.courseType == 1 ? "单课程" : "系列课") : "",
                  rules: [{ required: true, message: '请选择课程类型' }],
                })(
                  <Select placeholder="请选择" onChange= {this.chooseCourseType}>
                    {
                      courseTypeList.map(item => {
                        return <Option key={item.value}>{item.text}</Option>
                      })
                    }
                  </Select>
                )}
              </FormItem>
              <FormItem label="课程ID">
                {getFieldDecorator(formField, {
                  initialValue: courseType == 1 ? (modalObj.courseId ? modalObj.courseId : "") : (modalObj.courseGroupId ? modalObj.courseGroupId : ""),
                  rules: [{ required: true, message: '请选择课程ID' }],
                })(
                  <Select
                    placeholder="请选择"
                    style={{ width: '100%' }}
                  >
                    {courseType == 1 && courseList.map(item => {
                      return <Option key={item.id}>{item.id}</Option>
                    })}
                    {courseType == 2 && courseGroupList.map(item => {
                      return <Option key={item.id}>{item.id}</Option>
                    })}
                  </Select>
                )}
              </FormItem>
              <FormItem label="人数上限">
                {getFieldDecorator('quotaCount', {
                  initialValue: modalObj.quotaCount ? modalObj.quotaCount : "",
                  rules: [{ required: true, message: '请输入人数上限' }],
                })(
                  <Input />
                )}
              </FormItem>
          </Form>
        </Modal>
      </PageHeaderWrapper>
    );
  }
}

export default FreeList;
