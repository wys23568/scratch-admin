import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { Row, Col, Card, Form, Input, Button, message, Badge, Divider } from 'antd';
import ServiceTable from '@/component/ServiceTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import ServiceModal from '@/component/ServiceModal';
import ServiceButton from '@/component/ServiceButton';
import ServiceFile from '@/component/ServiceFile';

import styles from './FeedbackList.less';

const ButtonGroup = Button.Group;
const FormItem = Form.Item;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');

const stateFilters = [
  // 状态(0待处理、1处理中 2已处理)
  { value: 0, text: '待处理', icon: 'warning', enable: true },
  { value: 1, text: '处理中', icon: 'processing', enable: true },
  { value: 2, text: '已处理', icon: 'success', enable: true },
];
const typeFilters = [
  // 类型(1教师、2学生)
  { value: 0, text: '现金', enable: false },
  { value: 1, text: '教师', enable: true },
  { value: 2, text: '学生', enable: true },
];

/* eslint react/no-multi-comp:0 */
@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
@Form.create()
class FeedbackList extends PureComponent {
  state = {
    modalObj: {},
    modalVisible: false,
    searchFormValues: {},
    searchPageList: {
      content: [],
      pageable: {},
    },
  };

  // @ts-ignore
  searchParams = {};

  searchState = -1;

  modalUrl = 'account/admin/feedback';

  modalDef = [
    {
      title: '订单号',
      name: 'orderNo',
      type: 'string',
      modifyDisable: true,
    },
    {
      title: '学生',
      name: 'studentName',
      type: 'string',
      modifyDisable: true,
    },
    {
      title: '课程',
      name: 'courseName',
      type: 'string',
      modifyDisable: true,
    },
    {
      title: '支付金额',
      name: 'payMoney',
      type: 'number',
      modifyDisable: true,
    },
    {
      title: '退款金额',
      name: 'refundMoney',
      type: 'number',
      step: 0.01,
    },
  ];

  columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      render: text => <span>{text}</span>,
    },
    {
      title: '用户姓名',
      dataIndex: 'type',
      render: (val, record) => (
        <span>
          [{typeFilters[val] ? typeFilters[val].text : ''}]
          {record.userInfo ? record.userInfo.name : ''}
          <br />
          {record.userInfo ? record.userInfo.phone : ''}
        </span>
      ),
    },
    {
      title: '联系方式',
      dataIndex: 'contact',
      render: (text, record) => <span>{text}</span>,
    },
    {
      title: '标题',
      dataIndex: 'title',
      render: val => <span>{val}</span>,
    },
    {
      title: '内容',
      dataIndex: 'content',
      render: val => {
        return <span>{val}</span>;
      },
      width: '30%',
    },
    {
      title: '图片',
      dataIndex: 'imgUrl',
      render: (text, record) => <ServiceFile fileType="image" fileUrl={text} />,
    },
    {
      title: '状态',
      dataIndex: 'state',
      render(val) {
        return stateFilters[val] ? (
          <Badge status={stateFilters[val].icon} text={stateFilters[val].text} />
        ) : (
          ''
        );
      },
    },
    {
      title: '操作',
      render: (text, record) => {
        if (record.state === 0) {
          return (
            <Fragment>
              <a onClick={() => this.beginDeal(record)}>开始处理</a>
              <Divider type="vertical" />
              <a onClick={() => this.successDeal(record)}>结束处理</a>
            </Fragment>
          );
        }
        if (record.state === 1) {
          return (
            <Fragment>
              <a onClick={() => this.successDeal(record)}>结束处理</a>
            </Fragment>
          );
        }
        return '';
      },
    },
  ];

  componentDidMount() {
    this.beginSearch({ pageTotal: 0 });
  }

  handleStateSearch = state => {
    this.searchState = state;
    this.beginSearch({ pageTotal: 0 });
  };

  beginSearch = params => {
    const { dispatch } = this.props;
    params.searchState = this.searchState;
    this.searchParams = params;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/search`, data: params },
      callback: response => {
        if (response.result) {
          this.setState({
            searchPageList: response.data,
          });
        } else {
          this.setState({
            searchPageList: {},
          });
        }
      },
    });
  };

  handleServiceTableSearchChange = (pagination, filtersArg, sorter) => {
    const { searchFormValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      pageNumber: pagination.current - 1,
      pageSize: pagination.pageSize,
      pageTotal: pagination.total,
      ...searchFormValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    this.beginSearch(params);
  };

  beginDeal = obj => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/update`, data: { feedbackId: obj.id, state: 1 } },
      callback: response => {
        if (response.result) {
          message.success('处理成功');
          this.beginSearch(this.searchParams);
        } else {
          message.error(response.error);
        }
      },
    });
  };

  successDeal = obj => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/update`, data: { feedbackId: obj.id, state: 2 } },
      callback: response => {
        if (response.result) {
          message.success('处理成功');
          this.beginSearch(this.searchParams);
        } else {
          message.error(response.error);
        }
      },
    });
  };

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      searchFormValues: {},
    });

    this.beginSearch({ pageTotal: 0 });
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleFormSearch = e => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };

      this.setState({
        searchFormValues: values,
      });
      this.beginSearch({ ...values, pageTotal: 0 });
    });
  };

  handleModalCancel = () => {
    this.handleModalVisible(false);
  };

  handleModalOk = obj => {
    const { modalObj, fieldsValue } = obj;
    const { dispatch } = this.props;
    if (modalObj.id === undefined) {
    } else {
      dispatch({
        type: 'service/adminApiCall',
        payload: {
          url: `${this.modalUrl}/refund`,
          data: {
            orderId: modalObj.id,
            studentId: modalObj.studentId,
            refundMoney: fieldsValue.refundMoney,
          },
        },
        callback: response => {
          if (response.result) {
            message.success('退款成功');
            this.beginSearch(this.searchParams);
            this.handleModalVisible(false);
          } else {
            message.error(response.error);
          }
        },
      });
    }
  };

  handleModalCreate = () => {
    this.setState({
      modalVisible: true,
      modalObj: {},
    });
  };

  handleModalUpdate = obj => {
    obj.studentName = obj.studentInfo.name;
    obj.courseName = obj.courseInfo.name;
    obj.refundMoney = obj.payMoney;
    this.setState({
      modalVisible: true,
      modalObj: obj,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  render() {
    const { loading } = this.props;
    const { modalVisible, modalObj, searchPageList } = this.state;

    return (
      <PageHeaderWrapper title="反馈管理">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <ServiceButton
                stateFilters={stateFilters}
                searchState={this.searchState}
                handleStateSearch={state => this.handleStateSearch(state)}
              />
            </div>
            <ServiceTable
              selectedRows={[]}
              loading={loading}
              data={searchPageList}
              columns={this.columns}
              onChangeSearch={this.handleServiceTableSearchChange}
            />
          </div>
        </Card>
        <ServiceModal
          title="退款"
          handleOK={this.handleModalOk}
          handleCancel={this.handleModalCancel}
          modalObj={modalObj}
          modalDef={this.modalDef}
          modalVisible={modalVisible}
        />
      </PageHeaderWrapper>
    );
  }
}

export default FeedbackList;
