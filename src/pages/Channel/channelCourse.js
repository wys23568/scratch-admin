import React, {PureComponent,Fragment } from 'react';
import {connect} from 'dva';
import { Button, Select, Card, Modal, Form, Row, Col, Input, message, Divider, } from 'antd';

import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import ServiceTable from '@/component/ServiceTable';
import ServiceButton from '@/component/ServiceButton';
import DisCountModal from '@/component/ServiceChannleCourse/discountModal';
import InfoModal from '@/component/ServiceChannleCourse/infoModal';
import AddModal from '@/component/ServiceChannleCourse/addModal';
import styles from './ActivityList.less';

const ButtonGroup = Button.Group;
const { Option } = Select;
const FormItem = Form.Item;
@connect( ({service,loading}) => ({
  service,
  loading: loading.models.service
}))
@Form.create()
class channelCourse extends PureComponent {
  state = {
    addModalVisible: false,
    pageList: {
      content: [],
      pageable: {},
    },
    couponModalVisible: false,
    couponList: [],
    discountIds: '',
    disCountInfo: {},
    discountModalVisible: false,
    channelCourseId: '',
    defaultCoupon: [],
    courseList: [],
    courseGroupList: [],
    courseType: 0,  // 选择课程类型 1单课程、2系列课
    modalObj: {},
    modalType: 1, // 1:新建 2:修改
    courseData: {}, // 课程ID查询到的课程信息
  }
  componentDidMount () {
    this.pageInit();  // 查询列表信息
    this.queryCoupon(); // 查询优惠券
    this.fetchCourseList();
    this.fetchCourseGroupList();
  }
  // 列表title
  columns = [
    { title: 'Id', dataIndex: 'id',width: '6%', },
    { title: '期次', dataIndex: 'courseIssue',width: '8%', },
    { title: '课程名称', dataIndex: 'courseName',width: '12%', },
    { title: '渠道价格', dataIndex: 'price',width: '8%', },
    { 
      title: '代金券', dataIndex: 'discountIds',
      render: (val,record) => (
        <Fragment>
          {record.discountInfoList ? record.discountInfoList.map(item => {
              return <a style={{marginRight: '10px'}} key={item.id} onClick={() => this.showDiscountInfo(true,item)}>{item.name} 、</a>
          }) : "暂无优惠券"}
        </Fragment>
      )
    },
    {
      title: '操作',
      render: (val) => (
        <Fragment>
          <a onClick={() => this.reWriteCourse(val)}>修改</a>
          <Divider type="vertical" />
          <a onClick={() => this.associatedCoupon(val)}>关联优惠券</a>
        </Fragment>
      ),
    },
  ];
  // 页面数据初始化
  pageInit = () => {
    const {dispatch, match} = this.props;
    const params = {
      pageNumber: 0,
      pageSize: 20,
      channelId: match.params.channelId
    }
    dispatch({
      type: 'service/haimawangApiCall',
      payload: {url: `account/admin/channel/course/list`, data: params},
      callback: res=> {
        if(res.result) {
          this.setState({pageList: res.data})
        }
      }
    })
  }
  // 新建-弹框控制
  showModal = () => {
    this.setState({
      courseType: 0,
      courseData: {},
      modalObj: {},
      addModalVisible: true,
      modalType: 1,
    })
  }
  hideModal = () => {
    this.setState({
      addModalVisible: false,
      modalObj: {},
      modalType: 1,
    })
  }
  // 新建课程 选择课程ID 获取当前课程信息
  courseChange = (key,data) => {
    this.setState({
      courseData: data.props.val,
    })
  }

  // 修改课程数据
  reWriteCourse = val => {
    this.setState({
      addModalVisible: true,
      courseType: val.courseType,
      modalObj: val,
      modalType: 2
    })
  }
  
  // 查看关联优惠券信息 modal
  showDiscountInfo = (flag,disCountInfo)=> {
    this.setState({
      discountModalVisible: flag,
      disCountInfo,
    })
  }

  // 查询优惠券列表
  queryCoupon = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: {url: 'account/admin/discount/list', data:{pageTotal: 0,pageSize: -1} },
      callback: res =>{
        if(res.result) {
          this.setState({
            couponList: res.data.content
          })
        } else {
          message.error(res.error);
        }
      }
    })
  }
  //  关联优惠券 弹窗show
  associatedCoupon = argus => {
    this.setState({
      couponModalVisible: true,
      channelCourseId: argus.id,
      discountIds: argus.discountIds,
      defaultCoupon: argus.discountIds ? argus.discountIds.split(",") : []
    })
  }
  // 关联优惠券 弹窗false
  handleModalCoupon = () => {
    this.setState({
      couponModalVisible: false,
      channelCourseId: '',
      discountIds: '',
      defaultCoupon: []
    })
  }
  // 选择优惠券
  handleChangeCoupon = e => {
    this.setState({
      defaultCoupon: e,
      discountIds: e.join(",")
    })
  }
  // 提交关联优惠券
  submitCouponInfo = () => {
    const { dispatch } = this.props;
    const { discountIds,channelCourseId } = this.state;
    const params = {
      channelCourseId,
      discountIds,
    }
    dispatch({
      type: 'service/haimawangApiCall',
      payload: {url: 'account/admin/channel/course/discount/save', data:params },
      callback: res =>{
        if(res.result) {
          this.pageInit();
          this.setState({
            couponModalVisible: false,
            channelCourseId: '',
          })
        } else {
          message.error(res.error);
        }
      }
    })
  }

  // 新增活动 - 课程类型：单课程 -> 课程ID 列表
  fetchCourseList = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `course/online/buy`, data: {} },
      callback: response => {
        if (response.result) {
          let courseList = [];
          response.data.map(item => {
            courseList = courseList.concat(item.courseInfoList);
          })
          this.setState({
            courseList,
          });
        } else {
          this.setState({
            courseList: [],
          });
        }
      },
    });
  }
  // 新增活动 - 课程类型：系列课 -> 课程ID 列表
  fetchCourseGroupList = () => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `course/online/buy4Group`, data: {} },
      callback: response => {
        if (response.result) {
          let courseGroupList = [];
          this.setState({
            courseGroupList:response.data,
          });
        } else {
          this.setState({
            courseGroupList: [],
          });
        }
      },
    });
  }
  // 新增活动 课程类型选择
  chooseCourseType = val => {
    this.setState({
      courseType: val
    })
  }
  
  render () {
    const { loading,form: { getFieldDecorator}, match } = this.props;
    const {
      pageList, // 页面初始化数据
      addModalVisible,couponModalVisible,discountModalVisible,  // 控制弹窗显示数据
      modalObj,courseData,courseType,courseList,courseGroupList,  // 新增课程数据 modal
      couponList,defaultCoupon, // 关联优惠券数据 modal
      disCountInfo, // 优惠券信息数据 modal
    } = this.state;
    const formItemLayout = {labelCol: { xs: { span: 24 }, sm: { span: 6 } },wrapperCol: { xs: { span: 24 }, sm: { span: 14 } },};
    const courseTypeList =[ { value: 1, text: '单课程' },{ value: 2, text: '系列课' } ]
    const formField = courseType == 1 ? "courseId" : "courseGroupId";
    return (
      <PageHeaderWrapper title={this.modalTitle}>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={this.showModal}>
                新建
              </Button>
              当前渠道：<b>{match.params.channelName}</b>
            </div>
            <ServiceTable
              selectedRows={[]}
              loading={loading}
              data={pageList}
              columns={this.columns}
            />
          </div>
        </Card>
        {/* 新增课程 */}
        <AddModal 
          modalVisible={addModalVisible}
          modalObj={modalObj}
          pageInitFn={this.pageInit}
          cancelFn={this.hideModal}
          chooseCourseTypeFn={this.chooseCourseType}
          courseChangeFn={this.courseChange}
          courseData={courseData}
          courseType={courseType}
          courseList={courseList}
          courseGroupList={courseGroupList}
          channelId={match.params.channelId}
        />
        {/* 关联优惠券 */}
        <DisCountModal
          modalVisible={couponModalVisible}
          defaultCoupon={defaultCoupon}
          couponList={couponList}
          handleChangeCoupon={this.handleChangeCoupon}
          handleModalCoupon={this.handleModalCoupon}
          submitCouponInfo={this.submitCouponInfo}
        />
        {/* 查看优惠券信息 */}
        <InfoModal
          modalVisible={discountModalVisible}
          disCountInfo={disCountInfo}
          changeInfoModalState={() => this.showDiscountInfo(false,{})}
        />
      </PageHeaderWrapper>
    );
  }
}


export default channelCourse;