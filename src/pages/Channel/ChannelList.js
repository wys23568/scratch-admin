import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import router from 'umi/router';
import { Card, Form, Button, message, Badge, Divider } from 'antd';

import { arrayToString, arrayToMap } from '@/services/service';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import ServiceTable from '@/component/ServiceTable';
import ServiceModal from '@/component/ServiceModal';
import ServiceButton from '@/component/ServiceButton';

import styles from './ChannelList.less';

const stateFilters = [
  { value: 0, text: '禁用', icon: 'error' },
  { value: 1, text: '启用', icon: 'success' },
];
const infoModalCon = [
  { title: '名字', name: 'name', type: 'string' },
  { title: '编号', name: 'number', type: 'string' },
  { title: '介绍', name: 'info', type: 'string' },
  { title: '地址', name: 'address', type: 'string' },
  { title: '描述', name: 'remark', type: 'string' },
]
const accountModalCon = [
  { title: '手机号', name: 'phone', type: 'string' },
  { title: '密码', name: 'password', type: 'string' },
]
const stateMaps = arrayToMap(stateFilters, 'value');

@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
@Form.create()
class ChannelList extends PureComponent {
  state = {
    modalVisible: false,
    formValues: {},
    searchPageList: {
      content: [],
      pageable: {},
    },
    modalObj: {},
    modalType: "",
  };

  // @ts-ignore
  searchParams = {};

  searchState = -1;

  modalTitle = '渠道';

  modalUrl = 'account/admin/channel';

  modalDef = [];

  columns = [
    { title: 'Id', dataIndex: 'id',width: '7em', render: (value, record) => <span>{value}</span> },
    { title: '名字', dataIndex: 'name',width: '15em', render: val => <span>{val}</span> },
    { title: '手机号', dataIndex: 'phone',width: '7em', render: val => <span>{val}</span> },
    { title: '编号', dataIndex: 'number',width: '7em',  render: (value, record) => <span>{value}</span> },
    { title: '介绍', dataIndex: 'info', render: (value, record) => <span>{value}</span> },
    { title: '地址', dataIndex: 'address', render: (value, record) => <span>{value}</span> },
    { title: '备注', dataIndex: 'remark', render: (value, record) => <span>{value}</span>, },
    {
      title: '状态',
      dataIndex: 'state',
      sorter: true,
      width: '10em',
      render: value => {
        return stateFilters[value] ? (
          <Badge status={stateFilters[value].icon} text={stateFilters[value].text} />
        ) : ('');
      },
    },
    {
      title: '操作',
      render: (value, record) => (
        <Fragment>
          <a onClick={() => this.handleModalUpdate(record)}>修改</a>
          <Divider type="vertical" />
          {record.state === 0 ? (
            <a onClick={() => this.handleStateUpdate(record.id, 1)}>启用</a>
          ) : (
            <a onClick={() => this.handleStateUpdate(record.id, 0)}>禁用</a>
          )}
          <Divider type="vertical" />
          <a onClick={() => router.push(`/channel/course/${record.id}/${record.name}`)}>关联课程</a>
          <Divider type="vertical" />
          <a onClick={() => this.setupMaster(record)}>设置账号</a>
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    this.beginSearch({ pageTotal: 0 });
  }
  // 数据初始化
  beginSearch = searchParams => {
    const { dispatch, match } = this.props;
    this.searchParams = searchParams;
    this.searchParams.searchState = this.searchState;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `${this.modalUrl}/list`, data: this.searchParams },
      callback: response => {
        if (response.result) {
          this.setState({
            searchPageList: response.data,
          });
        } else {
          this.setState({
            searchPageList: {},
          });
        }
      },
    });
  };
  // 新建渠道
  handleModalCreate = () => {
    this.modalDef = infoModalCon;
    this.setState({
      modalVisible: true,
      modalObj: {},
      modalType: "add",
    });
  };
  // 修改渠道
  handleModalUpdate = obj => {
    this.modalDef = infoModalCon;
    this.setState({
      modalVisible: true,
      modalObj: obj,
      modalType: "update",
    });
  };
  // 关闭弹窗
  handleModalCancel = () => {
    this.setState({
      modalVisible: false,
      modalType: "",
      modalType: {} 
    });
  };
  // fetch 弹窗内容提交
  handleModalOk = obj => {
    const { modalObj, fieldsValue } = obj;
    const { dispatch } = this.props;
    const { modalType } = this.state;
    // const params = { ...fieldsValue };
    const params = { ...fieldsValue }
    if(modalType == "add") {
      const params = { ...fieldsValue };
      this.pushData(modalType,params,"新建成功")
    } else if (modalType == "update") {
      const params = { channelId: modalObj.id, ...fieldsValue };
      this.pushData(modalType,params,"更新成功")
    } else if (modalType == "updateLogin") {
      const params = { channelId: modalObj.id, ...fieldsValue };
      this.pushData(modalType,params,"设置成功")
    }
  };
  pushData = (modalType,params,msg) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `${this.modalUrl}/${modalType}`, data: { ...params } },
      callback: response => {
        if (response.result) {
          message.success(msg);
          this.beginSearch(this.searchParams);
          this.handleModalCancel();
        } else {
          message.error(response.error);
        }
      },
    });
  }
  // 状态修改
  handleStateUpdate = (channelId, state) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `${this.modalUrl}/update`, data: { channelId, state } },
      callback: response => {
        if (response.result) {
          message.success('设置成功');
          this.beginSearch(this.searchParams);
        } else {
          message.error(response.error);
        }
      },
    });
  };
  // 状态筛选
  handleStateSearch = state => {
    this.searchState = state;
    this.searchParams.pageTotal = 0;
    this.beginSearch(this.searchParams);
  };
  // 列表分页
  handleTableSearch = (pagination, filters, sorter) => {
    const { formValues } = this.state;
    const params = {
      pageNumber: pagination.current - 1,
      pageSize: pagination.pageSize,
      pageTotal: pagination.total,
      ...formValues,
      filters,
      sorter,
    };
    this.beginSearch(params);
  };
  // fetch设置账号
  setupMaster = val => {
    this.modalDef = accountModalCon;
    this.setState({
      modalVisible: true,
      modalObj: val,
      modalType: "updateLogin",
    });
  }

  render() {
    const { loading } = this.props;
    const { modalVisible, modalObj, searchPageList } = this.state;

    return (
      <PageHeaderWrapper title={this.modalTitle}>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalCreate()}>
                新建
              </Button>
              <ServiceButton
                stateFilters={stateFilters}
                searchState={this.searchState}
                handleStateSearch={state => this.handleStateSearch(state)}
              />
            </div>
            <ServiceTable
              rowKey="id"
              selectedRows={[]}
              loading={loading}
              data={searchPageList}
              columns={this.columns}
              onChangeSearch={this.handleTableSearch}
            />
          </div>
          <ServiceModal
            title={this.modalTitle}
            modalDef={this.modalDef}
            modalVisible={modalVisible}
            modalObj={modalObj}
            handleOK={this.handleModalOk}
            handleCancel={this.handleModalCancel}
          />
        </Card>
      </PageHeaderWrapper>
    );
  }
}

export default ChannelList;
