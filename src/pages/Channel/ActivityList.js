import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import router from 'umi/router';
import { Row,Col,Card,Form,Select,Icon,Button,Dropdown,Menu,message,Badge,Divider,} from 'antd';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import ServiceTable from '@/component/ServiceTable';
import ServiceModal from '@/component/ServiceModal';
import ServiceButton from '@/component/ServiceButton';
import ServiceFile from '@/component/ServiceFile';

import styles from './ActivityList.less';

const ButtonGroup = Button.Group;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = (obj, char) =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(char);
const stateFilters = [
  { value: 0, text: '禁用', icon: 'default' },
  { value: 1, text: '下线', icon: 'error' },
  { value: 2, text: '上线', icon: 'success' },
];

const courseTypeFilters = [
  { value: 0, text: '禁用', icon: 'error' },
  { value: 1, text: '单课', icon: 'success' },
  { value: 2, text: '年课', icon: 'success' },
];
/* eslint react/no-multi-comp:0 */
@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
@Form.create()
class ActivityList extends PureComponent {
  state = {
    modalVisible: false,
    selectedRows: [],
    formValues: {},
    searchPageList: {
      content: [],
      pageable: {},
    },
    modalObj: {},
  };

  // @ts-ignore
  searchParams = {};

  searchState = -1;

  modalTitle = '渠道统计';

  modalUrl = 'account/admin/channel/activity';

  modalDef = [
    { title: '名字', name: 'name', type: 'string' },
    { title: '目的', name: 'dest', type: 'string' },
    { title: '介绍', name: 'intro', type: 'string' },
    { title: '人数', name: 'quotaCount', type: 'string' },
    { title: '领课类型', name: 'courseType', type: 'string' },
    { title: '领课id', name: 'courseId', type: 'string' },
    { title: '活动图片', name: 'imgUrl', type: 'string' },
    { title: '描述', name: 'remark', type: 'string' },
  ];

  modalSearch = [
    { title: '名字', name: 'name', type: 'string' },
    { title: '目的', name: 'dest', type: 'string' },
  ];

  modalTable = [
    { title: 'Id', dataIndex: 'id' },
    { title: '活动类型', dataIndex: 'activityType' },
    { title: '活动Id', dataIndex: 'activityId' },
    {
      title: '状态',
      dataIndex: 'state',
      sorter: true,
      render: (value, record) => {
        return stateFilters[value] ? (
          <Badge status={stateFilters[value].icon} text={stateFilters[value].text} />
        ) : (
          ''
        );
      },
    },
    {
      title: '操作',
      render: (value, record) => (
        <Fragment>
          <a onClick={() => this.handleModalUpdate(record)}>修改</a>
          <Divider type="vertical" />
          {record.state !== 2 ? (
            <a onClick={() => this.handleStateUpdate(record.id, 2)}>上线</a>
          ) : (
            <a onClick={() => this.handleStateUpdate(record.id, 1)}>下线</a>
          )}
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    this.beginSearch({ pageTotal: 0 });
  }

  beginSearch = searchParams => {
    const { dispatch, match } = this.props;
    this.searchParams = searchParams;
    this.searchParams.searchState = this.searchState;
    this.searchParams.channelId = match.params.channelId;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/search`, data: this.searchParams },
      callback: response => {
        if (response.result) {
          this.setState({
            searchPageList: response.data,
          });
        } else {
          this.setState({
            searchPageList: {},
          });
        }
      },
    });
  };

  handleModalCreate = () => {
    this.setState({
      modalVisible: true,
      modalObj: {},
    });
  };

  handleModalUpdate = obj => {
    this.setState({
      modalVisible: true,
      modalObj: obj,
    });
  };

  handleModalCancel = () => {
    this.setState({ modalVisible: false });
  };

  handleModalOk = obj => {
    const { modalObj, fieldsValue } = obj;
    const { dispatch, match } = this.props;
    const params = { ...fieldsValue };
    if (modalObj.id === undefined) {
      dispatch({
        type: 'service/adminApiCall',
        payload: { url: `${this.modalUrl}/this/create`, data: { ...params } },
        callback: response => {
          if (response.result) {
            message.success('添加成功');
            this.beginSearch(this.searchParams);
            this.setState({ modalVisible: false });
          } else {
            message.error('添加失败');
          }
        },
      });
    } else {
      dispatch({
        type: 'service/adminApiCall',
        payload: {
          url: `${this.modalUrl}/this/update`,
          data: { activityFreeId: modalObj.id, ...params },
        },
        callback: response => {
          if (response.result) {
            message.success('修改成功');
            this.beginSearch(this.searchParams);
            this.setState({ modalVisible: false });
          } else {
            message.error('修改失败');
          }
        },
      });
    }
  };

  handleStateUpdate = (id, state) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/update`, data: { channelId: id, state: state } },
      callback: response => {
        if (response.result) {
          message.success('设置成功');
          this.beginSearch(this.searchParams);
        } else {
          message.error('设置失败');
        }
      },
    });
  };

  handleStateSearch = state => {
    this.searchState = state;
    this.searchParams.pageTotal = 0;
    this.beginSearch(this.searchParams);
  };

  handleModalSearch = state => {
    this.searchParams.pageTotal = 0;
    this.beginSearch(this.searchParams);
  };

  handleTableSearch = (pagination, filtersArg, sorter) => {
    const { formValues } = this.state;
    const filtersArray = [];
    const filtersKeys = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = '' + key + ':' + getValue(filtersArg[key], ',');
      filtersArray.push(newObj[key]);
      return newObj;
    }, {});
    const filters = filtersArray.join(';');
    const params = {
      pageNumber: pagination.current - 1,
      pageSize: pagination.pageSize,
      pageTotal: pagination.total,
      ...formValues,
      filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }
    this.beginSearch(params);
  };

  render() {
    const { loading } = this.props;
    const { selectedRows, modalVisible, modalObj, searchPageList } = this.state;

    return (
      <PageHeaderWrapper title={this.modalTitle}>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalCreate()}>
                新建
              </Button>
              <ServiceButton
                stateFilters={stateFilters}
                searchState={this.searchState}
                handleStateSearch={state => this.handleStateSearch(state)}
              />
            </div>
            <ServiceTable
              selectedRows={selectedRows}
              loading={loading}
              data={searchPageList}
              columns={this.modalTable}
              onChange={this.handleTableSearch}
            />
          </div>
        </Card>
        <ServiceModal
          title={this.modalTitle}
          modalDef={this.modalDef}
          modalVisible={modalVisible}
          modalObj={modalObj}
          handleOK={this.handleModalOk}
          handleCancel={this.handleModalCancel}
        />
      </PageHeaderWrapper>
    );
  }
}

export default ActivityList;
