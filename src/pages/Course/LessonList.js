import React, { Component, Fragment } from 'react';
import { connect } from 'dva';
import { Form, Card, Badge, Divider, Table, message, Tag } from 'antd';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import DescriptionList from '@/components/DescriptionList';
import ServiceModal from '@/component/ServiceModal';
import ServiceFile from '@/component/ServiceFile';

import moment from 'moment';
import router from 'umi/router';
import styles from './LessonList.less';

const { Description } = DescriptionList;

const stateFilters = [
  { value: 0, text: '正常', icon: 'success' },
  { value: 1, text: '未分配老师', icon: 'error' },
  { value: 2, text: '已分配老师', icon: 'success' },
];
const sourceTypeFilters = [{ value: 0, text: '线上' }, { value: 1, text: '线下' }];

/* eslint react/no-multi-comp:0 */
@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
@Form.create()
class LessonList extends Component {
  state = {
    modalVisible: false,
    modalObj: {},
    modalDetail: { coursePackageInfo: {}, teacherInfoList: [] },
    searchPageList: {
      content: [],
      pageable: {},
    },
    searchTotalList: [],
  };

  // @ts-ignore
  searchParams = {};

  modalUrl = 'account/admin/course/lesson';

  courseUrl = 'account/admin/course';

  modalDef = [
    {
      title: '名称',
      name: 'name',
      type: 'string',
      modifyDisable: true,
    },
    {
      title: '知识点',
      name: 'knowledges',
      type: 'text',
      list: [],
    },
    {
      title: '简短知识点',
      name: 'shortKnowledges',
      type: 'text',
      list: [],
    },
    {
      title: '开始时间',
      name: 'startAt',
      type: 'datetime',
    },
    {
      title: '持续分钟',
      name: 'timeMinute',
      type: 'integer',
    },
  ];

  columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      render: text => <span>{text}</span>,
    },
    {
      title: '名称',
      dataIndex: 'name',
      render: text => <span>{text}</span>,
    },
    {
      title: '知识点',
      dataIndex: 'knowledges',
      render: (text, record) => (
        <div style={{ wordWrap: 'break-word', wordBreak: 'break-all' }}>{record.knowledges}</div>
      ),
      width: '20%',
    },
    {
      title: '简短知识点',
      dataIndex: 'shortKnowledges',
      render: (text, record) => (
        <div style={{ wordWrap: 'break-word', wordBreak: 'break-all' }}>
          {record.shortKnowledges}
        </div>
      ),
      width: '20%',
    },
    {
      title: '时间',
      dataIndex: 'startAt',
      render: (text, record) => (
        <span>
          开始 : {moment(record.startAt).format('YYYY-MM-DD HH:mm')} <br /> 结束 :{' '}
          {moment(record.endAt).format('YYYY-MM-DD HH:mm')} <br /> 共{record.timeMinute}分钟
        </span>
      ),
    },
    {
      title: '视频',
      dataIndex: 'videoUrl',
      key: 'videoUrl',
      render: (text, record) => <ServiceFile fileType="video" fileUrl={text} />,
    },
    {
      title: '课件',
      dataIndex: 'docUrl',
      key: 'docUrl',
      render: text => {
        return text ? <a onClick={() => window.open(text)}>查看课件</a> : '';
      },
    },
    {
      title: '讲义',
      dataIndex: 'textbookUrl',
      key: 'textbookUrl',
      render: text => {
        return text ? <a onClick={() => window.open(text)}>查看讲义</a> : '';
      },
    },
    {
      title: '操作',
      render: (text, record) => (
        <Fragment>
          {record.state === 0 ? (
            <a onClick={() => this.handleModalUpdate(record)}>编辑信息</a>
          ) : null}
          {record.state === 0 ? <Divider type="vertical" /> : null}
          <a onClick={() => this.handleUpdateReset(record)}>同步信息</a>
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    this.beginSearch({ pageTotal: 0 });
    this.getCourseDeatil({ pageSize: 100, state: 0 });
  }

  getCourseDeatil = () => {
    const { dispatch, match } = this.props;
    const { params } = match;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.courseUrl}/this/detail`, data: { courseId: params.id } },
      callback: response => {
        if (response.result) {
          this.setState({
            modalDetail: response.data,
          });
        } else {
          this.setState({
            modalDetail: {},
          });
        }
      },
    });
  };

  beginSearch = params => {
    const { dispatch, match } = this.props;
    this.searchParams = params;
    dispatch({
      type: 'service/adminApiCall',
      payload: {
        url: `${this.modalUrl}/this/search`,
        data: { ...params, courseId: match.params.id },
      },
      callback: response => {
        if (response.result) {
          this.setState({
            searchTotalList: response.data,
          });
        } else {
          this.setState({
            searchTotalList: [],
          });
        }
      },
    });
  };

  previewItem = id => {
    router.push(`/profile/basic/${id}`);
  };

  handleUpdateReset = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: {
        url: `${this.modalUrl}/this/update`,
        data: { lessonId: record.id, videoUrl: 'reset', docUrl: 'reset' },
      },
      callback: response => {
        if (response.result) {
          message.success('重置成功');
          this.beginSearch(this.searchParams);
        } else {
          message.error('重置失败');
        }
      },
    });
  };

  handleUpdateJiefeng = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/update`, data: { lessonId: record.id, state: 0 } },
      callback: response => {
        if (response.result) {
          message.success('修改成功');
          this.beginSearch(this.searchParams);
        } else {
          message.error('修改失败');
        }
      },
    });
  };

  handleUpdateFengjin = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/update`, data: { lessonId: record.id, state: 1 } },
      callback: response => {
        if (response.result) {
          message.success('修改成功');
          this.beginSearch(this.searchParams);
        } else {
          message.error('修改失败');
        }
      },
    });
  };

  handleModalCancel = () => {
    this.handleModalVisible(false);
  };

  handleModalOk = obj => {
    const { modalObj, fieldsValue } = obj;
    const { dispatch } = this.props;
    if (modalObj.id === undefined) {
    } else {
      fieldsValue.startAt = fieldsValue.startAt.valueOf();
      dispatch({
        type: 'service/adminApiCall',
        payload: {
          url: `${this.modalUrl}/this/update`,
          data: { lessonId: modalObj.id, courseId: modalObj.courseId, ...fieldsValue },
        },
        callback: response => {
          if (response.result) {
            message.success('修改成功');
            this.beginSearch(this.searchParams);
            this.handleModalVisible(false);
          } else {
            message.error('修改失败');
          }
        },
      });
    }
  };

  handleModalCreate = () => {
    this.setState({
      modalVisible: true,
      modalObj: {},
    });
  };

  handleModalUpdate = obj => {
    this.setState({
      modalVisible: true,
      modalObj: obj,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  render() {
    const { loading } = this.props;
    const { modalVisible, modalObj, modalDetail, searchTotalList } = this.state;

    const mainSearch = (
      <DescriptionList size="large" title="" style={{ marginBottom: 32 }}>
        <Description term="名称">{modalDetail.name}</Description>
        <Description term="课程包">{modalDetail.coursePackageInfo.name}</Description>
        <Description term="老师">
          {modalDetail.teacherInfoList
            ? modalDetail.teacherInfoList.map(teacher => <Tag key={teacher.id}>{teacher.name}</Tag>)
            : ''}
        </Description>
        <Description term="报名时间">
          {moment(modalDetail.signStartAt).format('YYYY-MM-DD')} 至{' '}
          {moment(modalDetail.signEndAt).format('YYYY-MM-DD')}
        </Description>
        <Description term="上课时间">
          {moment(modalDetail.teachStartAt).format('YYYY-MM-DD')} 至{' '}
          {moment(modalDetail.teachEndAt).format('YYYY-MM-DD')}
        </Description>
      </DescriptionList>
    );

    return (
      <PageHeaderWrapper title="课程信息" content={mainSearch}>
        <Card bordered={false}>
          <div className={styles.tableList}>
            <Table
              rowKey="id"
              loading={loading}
              dataSource={searchTotalList}
              columns={this.columns}
            />
          </div>
        </Card>
        <ServiceModal
          title="学生"
          handleOK={this.handleModalOk}
          handleCancel={this.handleModalCancel}
          modalObj={modalObj}
          modalDef={this.modalDef}
          modalVisible={modalVisible}
        />
      </PageHeaderWrapper>
    );
  }
}

export default LessonList;
