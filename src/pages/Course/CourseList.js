import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import router from 'umi/router';
import {
  Row,
  Col,
  Card,
  Form,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  message,
  Badge,
  Divider,
} from 'antd';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import ServiceTable from '@/component/ServiceTable';
import ServiceModal from '@/component/ServiceModal';
import ServiceButton from '@/component/ServiceButton';

import styles from './CourseList.less';

const ButtonGroup = Button.Group;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');
const stateFilters = [
  { value: 0, text: '未完善', icon: 'success' },
  { value: 1, text: '待上线', icon: 'success' },
  { value: 2, text: '已上线', icon: 'success' },
  { value: 3, text: '报名中', icon: 'success' },
  { value: 4, text: '报名截止', icon: 'success' },
  { value: 5, text: '课程进行中', icon: 'success' },
  { value: 6, text: '课程结束', icon: 'success' },
];
const courseTypeFilters = [
  { value: 0, text: '线下课' },
  { value: 1, text: '单课' },
  { value: 2, text: '年课' },
];
const courseFlagList = [
  { value: 1, text: '可购买' },
  { value: 2, text: '不可购买' },
]

/* eslint react/no-multi-comp:0 */
@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
@Form.create()
class CourseList extends PureComponent {
  state = {
    modalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    packageList: [],
    groupList: [],
    teacherList: [],
    searchPageList: {
      content: [],
      pageable: {},
    },
    modalObj: {},
    loading: false,
  };

  // @ts-ignore
  searchParams = {};

  searchState = -1;

  modalUrl = 'account/admin/course';

  modalDef = [
    {
      title: '课程包',
      name: 'coursePackageId',
      type: 'selectSingle',
      list: [],
      modifyDisable: true,
    },
    {
      title: '年课',
      name: 'courseGroupId',
      type: 'selectSingle',
      list: [],
      modifyDisable: true,
      required: false,
    },
    {
      title: '期次',
      name: 'issue',
      type: 'string',
    },
    {
      title: '名称',
      name: 'name',
      type: 'string',
    },
    {
      title: '课程类别',
      name: 'courseType',
      type: 'selectSingle',
      list: courseTypeFilters,
    },
    {
      title: '报名时间',
      name: 'signTime',
      type: 'rangedatetime',
      times: [],
    },
    {
      title: '上课时间',
      name: 'teachTime',
      type: 'rangedatetime',
      times: [],
    },
    {
      title: '上课频次(周)',
      name: 'lessonDescription',
      type: 'string',
    },
    {
      title: '报名人数',
      name: 'enrollCount',
      type: 'number',
    },
    {
      title: '原价',
      name: 'price',
      type: 'number',
      step: 0.01,
    },
    {
      title: '真实价格',
      name: 'realPrice',
      type: 'number',
      step: 0.01,
    },
    {
      title: '封面',
      name: 'coverUrl',
      type: 'image',
    },
    {
      title: '是否可购买',
      name: 'courseFlag',
      type: 'selectSingle',
      list: courseFlagList,
    },
  ];

  ModalPackageItem = this.modalDef[0];

  ModalGroupItem = this.modalDef[1];

  ModalTeacherIdsItem = this.modalDef[2];

  ModalSignTimeItem = this.modalDef[5];

  ModalTeachTimeItem = this.modalDef[6];

  columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      render: (text, record) => <span>{text}</span>,
    },
    {
      title: '名称',
      dataIndex: 'name',
      render: (text, record) => (
        <span>
          [{record.coursePackageInfo.name}]
          <br />
          {text}
        </span>
      ),
    },
    {
      title: '信息',
      dataIndex: 'coursePackageId',
      render: (text, record) => (
        <span>
          {record.issue}期
          {courseTypeFilters[record.courseType] ? courseTypeFilters[record.courseType].text : ''}
          <br />
          年课信息:
          {record.courseGroupInfo ? record.courseGroupInfo.name : '无'}
          <br />
          招生人数:
          {record.enrollCount === null || record.enrollCount === 0 ? '不限' : record.enrollCount}
        </span>
      ),
    },
    {
      title: '报名时间',
      dataIndex: 'signStartAt',
      render: (start, record) => (
        <span>
          {moment(start).format('YYYY-MM-DD')} <br /> 到<br />{' '}
          {moment(record.signEndAt).format('YYYY-MM-DD')}
        </span>
      ),
    },
    {
      title: '上课时间',
      dataIndex: 'teachStartAt',
      render: (start, record) => (
        <span>
          {moment(start).format('YYYY-MM-DD')} <br /> 到<br />{' '}
          {moment(record.teachEndAt).format('YYYY-MM-DD')}
        </span>
      ),
    },
    {
      title: '价格',
      dataIndex: 'realPrice',
      render: (start, record) => (
        <span>
          原价 : {record.price}元 <br /> 现价 : {record.realPrice}元{' '}
        </span>
      ),
    },
    {
      title: '状态',
      dataIndex: 'state',
      render(val) {
        return stateFilters[val] ? (
          <Badge status={stateFilters[val].icon} text={stateFilters[val].text} />
        ) : (
          ''
        );
      },
    },
    {
      title: '操作',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleModalUpdate(record)}>修改信息</a>
          <Divider type="vertical" />
          <a onClick={() => this.previewItem(record.id)}>编辑课时</a>
          {record.state === 1 ? <Divider type="vertical" /> : null}
          {record.state === 1 ? <a onClick={() => this.onlineCourse(record.id)}>上线</a> : null}
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    this.beginSearch({ pageTotal: 0 });
    this.beginSearchPackage({ pageSize: 100, state: 1 });
    this.beginSearchGroup({ pageSize: 100, state: 1 });
    this.beginSearchTeacher({ pageSize: 100, state: 0 });
  }

  beginSearchTeacher = params => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: 'account/admin/teacher/this/search', data: params },
      callback: response => {
        if (response.result) {
          this.ModalTeacherIdsItem.list = [...response.data.content];
          this.setState({ teacherList: [...response.data.content] });
        } else {
          this.setState({ teacherList: [] });
        }
      },
    });
  };

  beginSearchGroup = params => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: 'account/admin/coursegroup/this/search', data: params },
      callback: response => {
        if (response.result) {
          this.ModalGroupItem.list = [...response.data.content];
          this.setState({ groupList: [...response.data.content] });
        } else {
          this.setState({ groupList: [] });
        }
      },
    });
  };

  beginSearchPackage = params => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `account/admin/coursepackage/this/search`, data: params },
      callback: response => {
        if (response.result) {
          this.ModalPackageItem.list = [...response.data.content];
          this.setState({ packageList: [...response.data.content] });
        } else {
          this.setState({ packageList: [] });
        }
      },
    });
  };

  handleStateSearch = state => {
    this.searchState = state;
    this.beginSearch({ pageTotal: 0 });
  };

  beginSearch = params => {
    this.setState({ loading: true });
    const { dispatch } = this.props;
    params.searchState = this.searchState;
    this.searchParams = params;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/search`, data: params },
      callback: response => {
        this.setState({ loading: false });
        if (response.result) {
          this.setState({
            searchPageList: response.data,
          });
        } else {
          this.setState({
            searchPageList: {},
          });
        }
      },
    });
  };

  handleServiceTableSearchChange = (pagination, filtersArg, sorter) => {
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      pageNumber: pagination.current - 1,
      pageSize: pagination.pageSize,
      pageTotal: pagination.total,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    this.beginSearch(params);
  };

  previewItem = id => {
    const { match } = this.props;
    router.push(`/course/scheduling/${id}`);
  };

  onlineCourse = id => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/update`, data: { courseId: id, state: 2 } },
      callback: response => {
        if (response.result) {
          message.success('上线成功');
          this.beginSearch(this.searchParams);
          this.handleModalVisible(false);
        } else {
          message.error('上线失败');
        }
      },
    });
  };

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });

    this.beginSearch({ pageTotal: 0 });
  };

  handleFormToggle = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    if (selectedRows.length === 0) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'service/adminApiCall',
          payload: {
            url: `${this.modalUrl}/this/search`,
            data: {
              key: selectedRows.map(row => row.key),
            },
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleFormSearch = e => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };

      this.setState({
        formValues: values,
      });
      this.beginSearch({ ...values, pageTotal: 0 });
    });
  };

  handleModalCancel = () => {
    this.handleModalVisible(false);
  };

  handleModalOk = obj => {
    const { modalObj, fieldsValue } = obj;
    const { dispatch } = this.props;

    const params = { ...fieldsValue };
    params.packageId = fieldsValue.coursePackageId;
    params.groupId = fieldsValue.courseGroupId;
    params.teachStartAt = fieldsValue.teachTime[0].valueOf();
    params.teachEndAt = fieldsValue.teachTime[1].valueOf();
    params.signStartAt = fieldsValue.signTime[0].valueOf();
    params.signEndAt = fieldsValue.signTime[1].valueOf();

    if (modalObj.id === undefined) {
      dispatch({
        type: 'service/adminApiCall',
        payload: { url: `${this.modalUrl}/this/create`, data: { ...params } },
        callback: response => {
          if (response.result) {
            message.success('添加成功');
            this.beginSearch(this.searchParams);
            this.handleModalVisible(false);
          } else {
            message.error('添加失败');
          }
        },
      });
    } else {
      dispatch({
        type: 'service/adminApiCall',
        payload: {
          url: `${this.modalUrl}/this/update`,
          data: { courseId: modalObj.id, ...params },
        },
        callback: response => {
          if (response.result) {
            message.success('修改成功');
            this.beginSearch(this.searchParams);
            this.handleModalVisible(false);
          } else {
            message.error('修改失败');
          }
        },
      });
    }
  };

  handleUpdateJiefeng = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/update`, data: { teacherId: record.id, state: 0 } },
      callback: response => {
        if (response.result) {
          message.success('修改成功');
          this.beginSearch(this.searchParams);
        } else {
          message.error('修改失败');
        }
      },
    });
  };

  handleUpdateFengjin = record => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/update`, data: { teacherId: record.id, state: 1 } },
      callback: response => {
        if (response.result) {
          message.success('修改成功');
          this.beginSearch(this.searchParams);
        } else {
          message.error('修改失败');
        }
      },
    });
  };

  handleModalCreate = () => {
    this.setState({
      modalVisible: true,
      modalObj: {},
    });
  };

  handleModalUpdate = obj => {
    this.ModalSignTimeItem.times[0] = obj.signStartAt;
    this.ModalSignTimeItem.times[1] = obj.signEndAt;
    this.ModalTeachTimeItem.times[0] = obj.teachStartAt;
    this.ModalTeachTimeItem.times[1] = obj.teachEndAt;
    this.setState({
      modalVisible: true,
      modalObj: obj,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };
    });
  };

  renderAdvancedForm() {
    const {
      form: { getFieldDecorator },
    } = this.props;
    const { packageList, groupList } = this.state;
    return (
      <Form onSubmit={this.handleFormSearch} layout="inline">
        <Row gutter={{ md: 8, lg: 24, xl: 48 }}>
          <Col md={8} sm={24}>
            <FormItem label="课程包">
              {getFieldDecorator('coursePackageId')(
                <Select placeholder="请选择" style={{ width: '100%' }}>
                  {packageList.map(item => (
                    <Option key={item.id}>{item.name}</Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <FormItem label="待上线年课">
              {getFieldDecorator('courseGroupId')(
                <Select placeholder="请选择" style={{ width: '100%' }}>
                  {groupList.map(item => (
                    <Option key={item.id}>{item.name}</Option>
                  ))}
                </Select>
              )}
            </FormItem>
          </Col>
          <Col md={8} sm={24}>
            <div style={{ overflow: 'hidden' }}>
              <div style={{ marginBottom: 24 }}>
                <Button type="primary" htmlType="submit">
                  查询
                </Button>
                <Button style={{ marginLeft: 8 }} onClick={this.handleFormReset}>
                  重置
                </Button>
              </div>
            </div>
          </Col>
        </Row>
      </Form>
    );
  }

  renderForm() {
    return this.renderAdvancedForm();
  }

  render() {
    const { loading } = this.state;
    const { selectedRows, modalVisible, modalObj, searchPageList } = this.state;

    const menu = (
      <Menu onClick={this.handleMenuClick} selectedKeys={[]}>
        <Menu.Item key="remove">删除</Menu.Item>
        <Menu.Item key="approval">批量审批</Menu.Item>
      </Menu>
    );

    return (
      <PageHeaderWrapper title="课程排期">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListForm}>{this.renderForm()}</div>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalCreate()}>
                新建
              </Button>
              {selectedRows.length > 0 && (
                <span>
                  <Button>批量操作</Button>
                  <Dropdown overlay={menu}>
                    <Button>
                      更多操作 <Icon type="down" />
                    </Button>
                  </Dropdown>
                </span>
              )}
              <ServiceButton
                stateFilters={stateFilters}
                searchState={this.searchState}
                handleStateSearch={state => this.handleStateSearch(state)}
              />
            </div>
            <ServiceTable
              selectedRows={selectedRows}
              loading={loading}
              data={searchPageList}
              columns={this.columns}
              onChangeSearch={this.handleServiceTableSearchChange}
            />
          </div>
        </Card>
        <ServiceModal
          title="课程"
          handleOK={this.handleModalOk}
          handleCancel={this.handleModalCancel}
          modalObj={modalObj}
          modalDef={this.modalDef}
          modalVisible={modalVisible}
        />
      </PageHeaderWrapper>
    );
  }
}

export default CourseList;
