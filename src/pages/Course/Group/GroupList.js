import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import router from 'umi/router';
import {
  Row,
  Col,
  Card,
  Form,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  message,
  Badge,
  Divider,
} from 'antd';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import ServiceTable from '@/component/ServiceTable';
import ServiceModal from '@/component/ServiceModal';
import ServiceButton from '@/component/ServiceButton';
import ServiceFile from '@/component/ServiceFile';

import styles from './GroupList.less';

const ButtonGroup = Button.Group;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');
const stateFilters = [
  { value: 0, text: '初始化', icon: 'default' },
  { value: 1, text: '待上线', icon: 'warning' },
  { value: 2, text: '上线', icon: 'success' },
  { value: 3, text: '禁用', icon: 'error' },
];

/* eslint react/no-multi-comp:0 */
@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
@Form.create()
class GroupList extends PureComponent {
  state = {
    modalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    searchPageList: {
      content: [],
      pageable: {},
    },
    modalObj: {},
  };

  // @ts-ignore
  searchParams = {};

  searchState = -1;

  modalUrl = 'account/admin/coursegroup';

  modalDef = [
    {
      title: '名字',
      name: 'name',
      type: 'string',
    },
    {
      title: '期次',
      name: 'issue',
      type: 'string',
    },
    {
      title: '原价',
      name: 'price',
      type: 'number',
      step: 1,
    },
    {
      title: '现价',
      name: 'realPrice',
      type: 'number',
      step: 1,
    },
    {
      title: '描述',
      name: 'remark',
      type: 'string',
    },
  ];

  columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      render: (text, record) => <span>{text}</span>,
    },
    {
      title: '标题',
      dataIndex: 'name',
      render: (text, record) => <span>{record.name}</span>,
    },
    {
      title: '期次',
      dataIndex: 'issue',
      render: (text, record) => <span>{record.issue}</span>,
    },
    {
      title: '原价',
      dataIndex: 'price',
      render: (start, record) => <span>{record.price}元</span>,
    },
    {
      title: '现价',
      dataIndex: 'realPrice',
      render: (start, record) => <span>{record.realPrice}元</span>,
    },
    {
      title: '状态',
      dataIndex: 'state',
      render(val) {
        return stateFilters[val] ? (
          <Badge status={stateFilters[val].icon} text={stateFilters[val].text} />
        ) : (
          ''
        );
      },
    },
    {
      title: '操作',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleModalUpdate(record)}>修改</a>
          <Divider type="vertical" />
          {record.state !== 2 ? (
            <a onClick={() => this.onlineCourse(record.id, 2)}>上线</a>
          ) : (
            <a onClick={() => this.onlineCourse(record.id, 3)}>禁用</a>
          )}
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    this.beginSearch({ pageTotal: 0 });
  }

  handleStateSearch = state => {
    this.searchState = state;
    this.beginSearch({ pageTotal: 0 });
  };

  beginSearch = searchparams => {
    const { dispatch, match } = this.props;
    searchparams.searchState = this.searchState;
    this.searchParams = searchparams;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/search`, data: searchparams },
      callback: response => {
        if (response.result) {
          this.setState({
            searchPageList: response.data,
          });
        } else {
          this.setState({
            searchPageList: {},
          });
        }
      },
    });
  };

  handleServiceTableSearchChange = (pagination, filtersArg, sorter) => {
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      pageNumber: pagination.current - 1,
      pageSize: pagination.pageSize,
      pageTotal: pagination.total,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    this.beginSearch(params);
  };

  onlineCourse = (id, state) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/update`, data: { groupId: id, state: state } },
      callback: response => {
        if (response.result) {
          message.success('设置成功');
          this.beginSearch(this.searchParams);
          this.handleModalVisible(false);
        } else {
          message.error('设置失败');
        }
      },
    });
  };

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });

    this.beginSearch({ pageTotal: 0 });
  };

  handleFormToggle = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    if (selectedRows.length === 0) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'service/adminApiCall',
          payload: {
            url: `${this.modalUrl}/this/search`,
            data: {
              key: selectedRows.map(row => row.key),
            },
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleFormSearch = e => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };

      this.setState({
        formValues: values,
      });
      this.beginSearch({ ...values, pageTotal: 0 });
    });
  };

  handleModalCancel = () => {
    this.handleModalVisible(false);
  };

  handleModalOk = obj => {
    const { modalObj, fieldsValue } = obj;
    const { dispatch, match } = this.props;

    const params = { ...fieldsValue };

    if (modalObj.id === undefined) {
      dispatch({
        type: 'service/adminApiCall',
        payload: { url: `${this.modalUrl}/this/create`, data: { ...params } },
        callback: response => {
          if (response.result) {
            message.success('添加成功');
            this.beginSearch(this.searchParams);
            this.handleModalVisible(false);
          } else {
            message.error('添加失败');
          }
        },
      });
    } else {
      dispatch({
        type: 'service/adminApiCall',
        payload: {
          url: `${this.modalUrl}/this/update`,
          data: { groupId: modalObj.id, ...params },
        },
        callback: response => {
          if (response.result) {
            message.success('修改成功');
            this.beginSearch(this.searchParams);
            this.handleModalVisible(false);
          } else {
            message.error('修改失败');
          }
        },
      });
    }
  };

  handleModalCreate = () => {
    this.setState({
      modalVisible: true,
      modalObj: {},
    });
  };

  handleModalUpdate = obj => {
    this.setState({
      modalVisible: true,
      modalObj: obj,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };
    });
  };

  render() {
    const { loading } = this.props;
    const { selectedRows, modalVisible, modalObj, searchPageList } = this.state;

    return (
      <PageHeaderWrapper title="年课">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalCreate()}>
                新建
              </Button>
              <ServiceButton
                stateFilters={stateFilters}
                searchState={this.searchState}
                handleStateSearch={state => this.handleStateSearch(state)}
              />
            </div>
            <ServiceTable
              selectedRows={selectedRows}
              loading={loading}
              data={searchPageList}
              columns={this.columns}
              onChange={this.handleServiceTableSearchChange}
            />
          </div>
        </Card>
        <ServiceModal
          title="年课"
          handleOK={this.handleModalOk}
          handleCancel={this.handleModalCancel}
          modalObj={modalObj}
          modalDef={this.modalDef}
          modalVisible={modalVisible}
        />
      </PageHeaderWrapper>
    );
  }
}

export default GroupList;
