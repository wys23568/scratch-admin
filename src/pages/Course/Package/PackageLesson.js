import React, { Component, Fragment } from 'react';
import { connect } from 'dva';
import { Card, Badge, Table, Divider, message } from 'antd';
import DescriptionList from '@/components/DescriptionList';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import ServiceModal from '@/component/ServiceModal';
import ServiceFile from '@/component/ServiceFile';
import styles from './PackageLesson.less';
import router from 'umi/router';

const { Description } = DescriptionList;

@connect(({ profile, loading }) => ({
  profile,
  loading: loading.models.service,
}))
class PackageLesson extends Component {
  state = {
    modalVisible: false,
    modalObj: {},
    modalDetail: {},
    modalLessons: [],
  };

  // @ts-ignore
  searchParams = {};

  modalUrl = 'account/admin/coursepackage/lesson';

  modalDef = [
    {
      title: '序号',
      name: 'number',
      type: 'integer',
    },
    {
      title: '名称',
      name: 'name',
      type: 'string',
    },
    {
      title: '标题',
      name: 'title',
      type: 'string',
    },
    {
      title: '知识点',
      name: 'knowledges',
      type: 'text',
    },
    {
      title: '简短知识点',
      name: 'shortKnowledges',
      type: 'text',
    },
    {
      title: '封面',
      name: 'coverUrl',
      type: 'image',
    },
    {
      title: '视频',
      name: 'videoUrl',
      type: 'string',
    },
    {
      title: '课件',
      name: 'docUrl',
      type: 'file',
    },
    {
      title: '讲义',
      name: 'textbookUrl',
      type: 'file',
    },
    {
      title: 'SB3',
      name: 'sb3Url',
      type: 'file',
    },
    {
      title: '标准代码',
      name: 'sb3Json',
      type: 'file',
      required: false,
    },
  ];

  modalColumns = [
    {
      title: 'Id',
      dataIndex: 'id',
      key: 'id',
      align: 'right',
      render: text => {
        return text;
      },
    },
    {
      title: '课名称',
      dataIndex: 'name',
      key: 'name',
      render: text => {
        return text;
      },
    },
    {
      title: '排序',
      dataIndex: 'number',
      key: 'number',
      // align: 'right',
      render: text => {
        return text;
      },
    },
    {
      title: '课标题',
      dataIndex: 'title',
      key: 'title',
      render: text => {
        return text;
      },
    },
    {
      title: '知识点',
      dataIndex: 'knowledges',
      render: (text, record) => (
        <div style={{ wordWrap: 'break-word', wordBreak: 'break-all' }}>{record.knowledges}</div>
      ),
      width: '20%',
    },
    {
      title: '简短知识点',
      dataIndex: 'shortKnowledges',
      render: (text, record) => (
        <div style={{ wordWrap: 'break-word', wordBreak: 'break-all' }}>
          {record.shortKnowledges}
        </div>
      ),
      width: '20%',
    },
    {
      title: 'SB3',
      dataIndex: 'sb3Url',
      key: 'sb3Url',
      render: text => {
        return text ? <a onClick={() => window.open(text)}>查看演示</a> : '';
      },
    },
    {
      title: '视频',
      dataIndex: 'videoUrl',
      key: 'videoUrl',
      render: (text, record) => <ServiceFile fileType="video" fileUrl={text} />,
    },
    {
      title: '课件',
      dataIndex: 'docUrl',
      key: 'docUrl',
      render: text => {
        return text ? <a onClick={() => window.open(text)}>查看课件</a> : '';
      },
    },
    {
      title: '讲义',
      dataIndex: 'textbookUrl',
      key: 'textbookUrl',
      render: text => {
        return text ? <a onClick={() => window.open(text)}>查看讲义</a> : '';
      },
    },
    {
      title: '操作',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleModalUpdate(record)}>修改课时</a>
          <Divider type="vertical" />
          <a onClick={() => this.previewItem(record.id)}>编辑视频</a>
        </Fragment>
      ),
    },
  ];

  packageUrl = 'account/admin/coursepackage';

  componentDidMount() {
    this.beginSearch();
  }

  previewItem = id => {
    const { match } = this.props;
    const { params } = match;
    router.push(`/course/package/${params.id}/video/${id}`);
  };

  beginSearch = () => {
    const { dispatch, match } = this.props;
    const { params } = match;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.packageUrl}/this/detail`, data: { packageId: params.id } },
      callback: response => {
        if (response.result) {
          this.setState({
            modalDetail: response.data,
            modalLessons: response.data.packageLessonInfo,
          });
        } else {
          this.setState({
            modalDetail: {},
            modalLessons: [],
          });
        }
      },
    });
  };

  handleModalUpdate = obj => {
    this.setState({
      modalVisible: true,
      modalObj: obj,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleModalCancel = () => {
    this.handleModalVisible(false);
  };

  handleModalOk = obj => {
    const { modalObj, fieldsValue } = obj;
    const { dispatch } = this.props;
    if (modalObj.id === undefined) {
      dispatch({
        type: 'service/adminApiCall',
        payload: { url: `${this.modalUrl}/this/create`, data: { ...fieldsValue } },
        callback: response => {
          if (response.result) {
            message.success('添加成功');
            this.beginSearch();
            this.handleModalVisible(false);
          } else {
            message.error('添加失败');
          }
        },
      });
    } else {
      dispatch({
        type: 'service/adminApiCall',
        payload: {
          url: `${this.modalUrl}/this/update`,
          data: { lessonId: modalObj.id, ...fieldsValue },
        },
        callback: response => {
          if (response.result) {
            message.success('修改成功');
            this.beginSearch();
            this.handleModalVisible(false);
          } else {
            message.error('修改失败');
          }
        },
      });
    }
  };

  render() {
    const { modalDetail, modalLessons, modalVisible, modalObj } = this.state;
    const { loading } = this.props;

    return (
      <PageHeaderWrapper title={`[课程包]${modalDetail.name}`} loading={loading}>
        <Card bordered={false}>
          <DescriptionList size="large" title="课程包信息" style={{ marginBottom: 32 }}>
            <Description term="Id">{modalDetail.id}</Description>
            <Description term="名称">{modalDetail.name}</Description>
            <Description term="课时数量">{modalDetail.lessonCount}</Description>
            <Description term="介绍">{modalDetail.remark}</Description>
          </DescriptionList>
          <Divider style={{ marginBottom: 32 }} />
          <div className={styles.title}>课程包课时</div>
          <Table
            rowKey="id"
            style={{ marginBottom: 16 }}
            pagination={false}
            loading={loading}
            dataSource={modalLessons}
            columns={this.modalColumns}
          />
        </Card>
        <ServiceModal
          title="课时"
          handleOK={this.handleModalOk}
          handleCancel={this.handleModalCancel}
          modalObj={modalObj}
          modalDef={this.modalDef}
          modalVisible={modalVisible}
        />
      </PageHeaderWrapper>
    );
  }
}

export default PackageLesson;
