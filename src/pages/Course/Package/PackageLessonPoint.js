import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import router from 'umi/router';
import {
  Row,
  Col,
  Card,
  Form,
  Select,
  Icon,
  Button,
  Dropdown,
  Menu,
  message,
  Badge,
  Divider,
} from 'antd';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';
import ServiceTable from '@/component/ServiceTable';
import ServiceModal from '@/component/ServiceModal';
import ServiceButton from '@/component/ServiceButton';
import ServiceFile from '@/component/ServiceFile';

import styles from './PackageLessonPoint.less';

const ButtonGroup = Button.Group;
const FormItem = Form.Item;
const { Option } = Select;
const getValue = obj =>
  Object.keys(obj)
    .map(key => obj[key])
    .join(',');
const stateFilters = [
  { value: 0, text: '禁用', icon: 'error' },
  { value: 1, text: '正常', icon: 'success' },
];

/* eslint react/no-multi-comp:0 */
@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
@Form.create()
class PackageLessonPoint extends PureComponent {
  state = {
    modalVisible: false,
    expandForm: false,
    selectedRows: [],
    formValues: {},
    searchPageList: {
      content: [],
      pageable: {},
    },
    modalObj: {},
  };

  // @ts-ignore
  searchParams = {};

  searchState = -1;

  modalUrl = 'account/admin/coursepackage/lesson/point';

  modalDef = [
    {
      title: '序号',
      name: 'number',
      type: 'number',
      step: 1,
    },
    {
      title: '开始时间(秒)',
      name: 'startTime',
      type: 'number',
      step: 1,
    },
    {
      title: '结束时间(秒)',
      name: 'endTime',
      type: 'number',
      step: 1,
    },
    {
      title: '标题',
      name: 'title',
      type: 'string',
    },
    {
      title: '描述',
      name: 'remark',
      type: 'string',
    },
    {
      title: '截图',
      name: 'imgUrl',
      type: 'image',
      required: false,
    },
  ];

  columns = [
    {
      title: 'Id',
      dataIndex: 'id',
      render: (text, record) => <span>{text}</span>,
    },
    {
      title: '标题',
      dataIndex: 'title',
      render: (text, record) => <span>{record.title}</span>,
    },
    {
      title: '序号',
      dataIndex: 'number',
      render: (text, record) => <span>{record.number}</span>,
    },
    {
      title: '开始时间(秒)',
      dataIndex: 'startTime',
      render: (start, record) => <span>{record.startTime}</span>,
    },
    {
      title: '结束时间(秒)',
      dataIndex: 'endTime',
      render: (start, record) => <span>{record.endTime}</span>,
    },
    {
      title: '截图',
      dataIndex: 'imgUrl',
      render: (imgUrl, record) => <ServiceFile fileType="image" fileUrl={imgUrl} />,
    },
    {
      title: '状态',
      dataIndex: 'state',
      render(val) {
        return stateFilters[val] ? (
          <Badge status={stateFilters[val].icon} text={stateFilters[val].text} />
        ) : (
          ''
        );
      },
    },
    {
      title: '操作',
      render: (text, record) => (
        <Fragment>
          <a onClick={() => this.handleModalUpdate(record)}>修改</a>
          <Divider type="vertical" />
          {record.state === 0 ? (
            <a onClick={() => this.onlineCourse(record.id, 1)}>启用</a>
          ) : (
            <a onClick={() => this.onlineCourse(record.id, 0)}>禁用</a>
          )}
        </Fragment>
      ),
    },
  ];

  componentDidMount() {
    this.beginSearch({ pageTotal: 0 });
  }

  handleStateSearch = state => {
    this.searchState = state;
    this.beginSearch({ pageTotal: 0 });
  };

  beginSearch = searchparams => {
    const { dispatch, match } = this.props;
    const { params } = match;
    searchparams.searchState = this.searchState;
    searchparams.packageId = params.packageId;
    searchparams.lessonId = params.lessonId;
    this.searchParams = searchparams;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/search`, data: searchparams },
      callback: response => {
        if (response.result) {
          this.setState({
            searchPageList: response.data,
          });
        } else {
          this.setState({
            searchPageList: {},
          });
        }
      },
    });
  };

  handleServiceTableSearchChange = (pagination, filtersArg, sorter) => {
    const { formValues } = this.state;

    const filters = Object.keys(filtersArg).reduce((obj, key) => {
      const newObj = { ...obj };
      newObj[key] = getValue(filtersArg[key]);
      return newObj;
    }, {});

    const params = {
      pageNumber: pagination.current - 1,
      pageSize: pagination.pageSize,
      pageTotal: pagination.total,
      ...formValues,
      ...filters,
    };
    if (sorter.field) {
      params.sorter = `${sorter.field}_${sorter.order}`;
    }

    this.beginSearch(params);
  };

  onlineCourse = (id, state) => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/adminApiCall',
      payload: { url: `${this.modalUrl}/this/update`, data: { pointId: id, state: state } },
      callback: response => {
        if (response.result) {
          message.success('设置成功');
          this.beginSearch(this.searchParams);
          this.handleModalVisible(false);
        } else {
          message.error('设置失败');
        }
      },
    });
  };

  handleFormReset = () => {
    const { form } = this.props;
    form.resetFields();
    this.setState({
      formValues: {},
    });

    this.beginSearch({ pageTotal: 0 });
  };

  handleFormToggle = () => {
    const { expandForm } = this.state;
    this.setState({
      expandForm: !expandForm,
    });
  };

  handleMenuClick = e => {
    const { dispatch } = this.props;
    const { selectedRows } = this.state;
    if (selectedRows.length === 0) return;
    switch (e.key) {
      case 'remove':
        dispatch({
          type: 'service/adminApiCall',
          payload: {
            url: `${this.modalUrl}/this/search`,
            data: {
              key: selectedRows.map(row => row.key),
            },
          },
          callback: () => {
            this.setState({
              selectedRows: [],
            });
          },
        });
        break;
      default:
        break;
    }
  };

  handleSelectRows = rows => {
    this.setState({
      selectedRows: rows,
    });
  };

  handleFormSearch = e => {
    e.preventDefault();
    const { form } = this.props;
    form.validateFields((err, fieldsValue) => {
      if (err) return;
      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };

      this.setState({
        formValues: values,
      });
      this.beginSearch({ ...values, pageTotal: 0 });
    });
  };

  handleModalCancel = () => {
    this.handleModalVisible(false);
  };

  handleModalOk = obj => {
    const { modalObj, fieldsValue } = obj;
    const { dispatch, match } = this.props;

    const params = { ...fieldsValue };
    params.lessonId = match.params.lessonId;

    if (modalObj.id === undefined) {
      dispatch({
        type: 'service/adminApiCall',
        payload: { url: `${this.modalUrl}/this/create`, data: { ...params } },
        callback: response => {
          if (response.result) {
            message.success('添加成功');
            this.beginSearch(this.searchParams);
            this.handleModalVisible(false);
          } else {
            message.error('添加失败');
          }
        },
      });
    } else {
      dispatch({
        type: 'service/adminApiCall',
        payload: {
          url: `${this.modalUrl}/this/update`,
          data: { pointId: modalObj.id, ...params },
        },
        callback: response => {
          if (response.result) {
            message.success('修改成功');
            this.beginSearch(this.searchParams);
            this.handleModalVisible(false);
          } else {
            message.error('修改失败');
          }
        },
      });
    }
  };

  handleModalCreate = () => {
    this.setState({
      modalVisible: true,
      modalObj: {},
    });
  };

  handleModalUpdate = obj => {
    this.setState({
      modalVisible: true,
      modalObj: obj,
    });
  };

  handleModalVisible = flag => {
    this.setState({
      modalVisible: !!flag,
    });
  };

  handleSearch = e => {
    e.preventDefault();

    const { dispatch, form } = this.props;

    form.validateFields((err, fieldsValue) => {
      if (err) return;

      const values = {
        ...fieldsValue,
        updatedAt: fieldsValue.updatedAt && fieldsValue.updatedAt.valueOf(),
      };
    });
  };

  render() {
    const { loading } = this.props;
    const { selectedRows, modalVisible, modalObj, searchPageList } = this.state;

    return (
      <PageHeaderWrapper title="视频点">
        <Card bordered={false}>
          <div className={styles.tableList}>
            <div className={styles.tableListOperator}>
              <Button icon="plus" type="primary" onClick={() => this.handleModalCreate()}>
                新建
              </Button>
              <ServiceButton
                stateFilters={stateFilters}
                searchState={this.searchState}
                handleStateSearch={state => this.handleStateSearch(state)}
              />
            </div>
            <ServiceTable
              selectedRows={selectedRows}
              loading={loading}
              data={searchPageList}
              columns={this.columns}
              onChange={this.handleServiceTableSearchChange}
            />
          </div>
        </Card>
        <ServiceModal
          title="视频点"
          handleOK={this.handleModalOk}
          handleCancel={this.handleModalCancel}
          modalObj={modalObj}
          modalDef={this.modalDef}
          modalVisible={modalVisible}
        />
      </PageHeaderWrapper>
    );
  }
}

export default PackageLessonPoint;
