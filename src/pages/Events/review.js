// page: 赛事审核
import React, { PureComponent, Fragment } from 'react';
import { connect } from 'dva';
import moment from 'moment';
import { Row, Card, Form, Input,InputNumber, Select, Radio, Button, message, Modal, Divider,Popconfirm } from 'antd';
import ServiceTable from '@/component/ServiceTable';
import PageHeaderWrapper from '@/components/PageHeaderWrapper';

import styles from './review.less';


@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))

class eventsReview extends PureComponent {
  state = {
    pageList: {
      content: [],
      pageable: {},
    },
  };
  
  columns = [
    {
      title: 'id',
      dataIndex: 'id',
      render: text => <span>{text}</span>,
    },
  ];

  componentDidMount() {
    this.initFn({ pageTotal: 0,pageSize: 10 });
  }
  // 初始化表格数据
  initFn = params => {
    const { dispatch } = this.props;
    dispatch({
      type: 'service/haimawangApiCall',
      payload: { url: `account/admin/discount/list`, data: params },
      callback: response => {
        if (response.result) {
          this.setState({
            searchPageList: response.data,
          });
        } else {
          this.setState({
            searchPageList: {},
          });
        }
      },
    });
  };
  
  render() {
    const { loading, } = this.props;
    const { pageList } = this.state;
    return (
      
      <PageHeaderWrapper title="赛事审核">
        <Card bordered={false}>
          <div className={styles.tableListOperator}>
            <Button icon="plus" type="primary" onClick={() => this.handleModalCreate(true,'add')}>
              新建
            </Button>
          </div>
          <div className={styles.tableList}>
            <ServiceTable
              rowKey="id"
              selectedRows={[]}
              loading={loading}
              data={pageList}
              columns={this.columns}
              onChangeSearch={this.handleTableSearch}
            />
          </div>
        </Card>
      </PageHeaderWrapper>
    )
  }
}

export default eventsReview;