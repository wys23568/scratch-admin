export default {
  'menu.home': '首页',
  'menu.login': '登录',
  'menu.register': '注册',
  'menu.register.result': '注册结果',
  'menu.user': '用户',
  
  'menu.user.student': '学生',
  'menu.user.teacher': '教师',

  'menu.student': '学生',
  'menu.teacher': '教师',

  'menu.channel': '渠道',
  'menu.channel.activity': '渠道活动',
  'menu.channel.course': '关联课程',

  'menu.channelStudent': '渠道学员管理',

  'menu.discount': '优惠券',

  'menu.feedback': '反馈管理',
  'menu.report': '举报管理',


  'menu.course': '课程管理',
  'menu.course.package': '课程包',
  'menu.course.group': '年课',
  'menu.course.scheduling': '课程排期',
  'menu.course.fenban': '课程分班',

  'menu.course.point': '视频点',
  'menu.course.fenbanstudent': '学生分班',
  'menu.course.fenbanteacher': '教师分班',
  'menu.course.lesson': '课时',
  'menu.course.lesson': '课时',
  'menu.course.package': '课程包',


  'menu.order': '订单管理',
  
  'menu.activity': '活动管理',
  'menu.activity.grouppurchase': '团购活动',
  'menu.activity.freeevents': '免费活动',

  'menu.events': '赛事管理',
  'menu.events.review': '赛事审核',
  'menu.events.configuration': '赛事配置',
  'menu.events.player': '选手管理',
  'menu.events.judge': '评委管理',
  
  'menu.exception': '异常页',
  'menu.exception.not-permission': '403',
  'menu.exception.not-find': '404',
  'menu.exception.server-error': '500',
  'menu.exception.trigger': '触发错误',

  'menu.account': '个人页',
  'menu.account.center': '个人中心',
  'menu.account.settings': '个人设置',
  'menu.account.trigger': '触发报错',
  'menu.account.logout': '退出登录',
};
