import React, { Component,PureComponent } from 'react';
import { connect } from 'dva';
import { Modal, Form, Input, Select, message, } from 'antd';
import styles from './index.less';

const FormItem = Form.Item;
const { Option } = Select;
@Form.create()

@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
class GroupPurchaseModal extends PureComponent {
  state = {
    chooseChannelList: [],
  }
  // 关联渠道数据
  associated = () => {
    const { form, dispatch, refreshTable,channelInfo,chooseChannelList } = this.props;
    // const { chooseChannelList } = this.state;
    let channleListArr = [];
    let pricesArr = [];
    chooseChannelList.map(item => {
      channleListArr.push(item.split("&")[0])
    })
    form.validateFields((err,values) => {
      console.log(err)
      if(err) {
        return false;
      }
      chooseChannelList.map((item,index) => {
        console.log(values["channelPrice" + index])
        pricesArr.push(values["channelPrice"+index])
      })
      const params = {
        activityId: channelInfo.id,
        activityType: 1,
        channelIds: channleListArr.join(","),
        prices: pricesArr.join(","),
      };
      dispatch({
        type: 'service/haimawangApiCall',
        payload: { url: `account/admin/channel/activity/save`, data: { ...params } },
        callback: response => {
          if (response.result) {
            message.success('关联渠道成功');
            refreshTable({ pageTotal: 0 });
            this.cancel();
          } else {
            message.error('添加失败');
          }
        },
      });
    })
  }
  cancel = () => {
    const { handleCancel } = this.props;
    handleCancel();
    
    const { form, } = this.props;
    form.resetFields();
    this.setState({
      chooseChannelList: [],
    })
  }
  render (){
    const { modalVisible,form: { getFieldDecorator },channelList,channelInfo,chooseChannelList,handleChildValueChange } = this.props;
    const formItemLayout = { labelCol: {xs: { span: 24 },sm: { span: 7 }}, wrapperCol: {xs: { span: 24 },sm: { span: 12 }} };
    return (
      <Modal
        title="关联渠道"
        centered
        visible={modalVisible}
        onOk={this.associated}
        onCancel={this.cancel}
        bodyStyle={{ maxHeight: '60vh',overflowY:'scroll'}}
      >
        <Form {...formItemLayout}>
          <FormItem label="渠道ID">
            {getFieldDecorator("channelIds", {
              initialValue: chooseChannelList && chooseChannelList.length > 0 ? chooseChannelList : [],
              rules: [{ required: true, message: '请选择渠道ID' }],
            })(
              <Select
                mode="multiple"
                placeholder="请选择"
                style={{ width: '100%' }}
                onChange={handleChildValueChange}
              >
                {channelList.map((item) => {
                  return <Option key={item.id+"&"+item.name}>{item.id}</Option>
                })}
                
              </Select>
            )}
          </FormItem>
          {chooseChannelList.map((item,index) => {
            return <div className={styles.channelBox} key={index}>
              <div><span>渠道{index + 1}：</span>{item.split("&")[0]}</div>
              <div><span>渠道名称：</span>{item.split("&")[1]}</div>
              <FormItem label={"渠道价格"+(index+1)+"(元)"}>
                {getFieldDecorator(`channelPrice${index}` , {
                  initialValue: channelInfo && channelInfo.channelActivityInfoList && channelInfo.channelActivityInfoList[index] ? channelInfo.channelActivityInfoList
                  [index].price : "",
                  rules: [{ required: true, message: '请输入当前渠道价格' }],
                })(
                  <Input style={{ width: '100%' }}/>
                )}
              </FormItem>
            </div>
          })}
        </Form>
      </Modal>
    )
  }
}

export default GroupPurchaseModal;