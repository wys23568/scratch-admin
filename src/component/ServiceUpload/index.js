import React, { Component } from 'react';
import { Button, Form, Icon, message, Modal, Row, Upload } from 'antd';
import { MiniArea } from '../../components/Charts';
import NumberInfo from '../../components/NumberInfo';
import styles from './index.less';

@Form.create()
class ServiceUpload extends Component {
  state = {
    previewVisible: false,
    previewUrl: '',
    fileList: [],
  };

  defaultFile = {
    uid: '-1',
    name: 'file',
    status: 'done',
    url: 'https://zos.alipayobjects.com/rmsportal/jkjgkEfvpUPVyRjUImniVslZfWPnJuuZ.png',
  };

  fileProps = {
    name: 'file',
    // action: `https://api.huazilive.com/api/service/account/common/upload?loginToken=${localStorage.getItem('loginToken')}`,
    action: `https://api.huazilive.com/api/service/account/common/upload?loginToken=${localStorage.getItem(
      'loginToken'
    )}`,
    headers: {
      authorization: 'authorization',
    },
  };

  componentDidMount() {
    const { fileUrl } = this.props;
    if (fileUrl !== null) {
      this.defaultFile.url = fileUrl;
    }
    this.setState({ fileList: [this.defaultFile] });
  }

  handleChange = info => {
    const { fileMax = 1, handleFilesChange, name } = this.props;

    // console.log(info.file, info.fileList);
    if (info.fileList === null || info.fileList.length === 0) {
      this.setState({ fileList: [this.defaultFile] });
      handleFilesChange({ name, url: this.defaultFile.url });
      return;
    }
    if (info.file.status === 'uploading') {
      this.setState({ fileList: info.fileList });
    } else if (info.file.status === 'removed') {
      this.setState({ fileList: info.fileList });
    } else {
      const count = fileMax > info.fileList.length ? info.fileList.length : fileMax;
      const newlist = [];
      for (let i = 0; i < count; i++) {
        newlist[i] = info.fileList[i];
      }
      if (info.file.status === 'done' && info.file.response.result === true) {
        // console.log(`${info.file.name} file uploaded done`);
        if (info.fileList.length >= fileMax) {
          newlist[fileMax - 1] = info.fileList[info.fileList.length - 1];
          this.setState({ fileList: newlist });
          handleFilesChange({ name, url: info.file.response.data.url });
        } else {
          this.setState({ fileList: info.fileList });
        }
      } else if (
        info.file.status === 'error' ||
        (info.file.uid !== '-1' && info.file.response.result !== true)
      ) {
        const file = info.fileList[info.fileList.length - 1];
        file.status = 'error';
        if (info.fileList.length >= fileMax) {
          newlist[fileMax] = info.fileList[info.fileList.length - 1];
          this.setState({ fileList: newlist });
        } else {
          this.setState({ fileList: info.fileList });
        }
      }
    }
  };

  handlePreview = file => {
    this.setState({
      previewUrl: file.url || file.thumbUrl,
      previewVisible: true,
    });
  };

  handleCancel = () => this.setState({ previewVisible: false });

  render() {
    const { fileType } = this.props;
    const { previewVisible, previewUrl, fileList } = this.state;

    let listType = 'text';
    let modelshow = <img alt="example" style={{ width: '100%' }} src={previewUrl} />;
    let uploadshow = (
      <div>
        <Icon type="plus" />
        <div className="ant-upload-text">Upload</div>
      </div>
    );
    if (fileType === 'image') {
      listType = 'picture-card';
      modelshow = <img alt="example" style={{ width: '100%' }} src={previewUrl} />;
      uploadshow = (
        <div>
          <Icon type="plus" />
          <div className="ant-upload-text">Upload</div>
        </div>
      );
    } else if (fileType === 'video') {
      listType = 'text';
      modelshow = (
        <video alt="example" style={{ width: '100%' }} src={previewUrl} controls="controls">
          您的浏览器不支持 video 标签。
        </video>
      );
      uploadshow = (
        <Button>
          <Icon type="upload" />
          Upload
        </Button>
      );
    } else {
      listType = 'text';
      modelshow = (
        <span>
          暂不支持浏览文件。您可以点击<a onClick={() => window.open(previewUrl)}>下载文件</a>。
        </span>
      );
      uploadshow = (
        <Button>
          <Icon type="upload" />
          Upload
        </Button>
      );
    }
    return (
      <Row>
        <Upload
          {...this.fileProps}
          listType={listType}
          fileList={fileList}
          onChange={this.handleChange}
          onPreview={this.handlePreview}
        >
          {uploadshow}
        </Upload>
        <Modal visible={previewVisible} footer={null} onCancel={this.handleCancel}>
          {modelshow}
        </Modal>
      </Row>
    );
  }
}

export default ServiceUpload;
