import React, { Component,PureComponent } from 'react';
import { connect } from 'dva';
import { Modal, Select, } from 'antd';

const { Option } = Select;

@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
class DiscountModal extends PureComponent {
  render (){
    const { modalVisible,defaultCoupon,couponList, submitCouponInfo,handleChangeCoupon,handleModalCoupon,  } = this.props;
    return (
      <Modal
        title="关联优惠券"
        centered
        visible={modalVisible}
        onOk={submitCouponInfo}
        onCancel={handleModalCoupon}
      >
        <div>
          <span style={{marginRight: '10px'}}>优惠券ID:   </span>
          <Select
            mode="multiple"
            placeholder="请选择优惠券"
            value={defaultCoupon}
            onChange={handleChangeCoupon}
            style={{ width: '60%' }}
          >
            {couponList.map(item => {
              return <Option key={item.id} label={item.id}>{item.name}</Option>
            })}
          </Select>
        </div>
      </Modal>
    )
  }
}

export default DiscountModal;