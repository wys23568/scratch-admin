import React, { Component,PureComponent } from 'react';
import { connect } from 'dva';
import { Modal, Select, } from 'antd';
import styles from './index.less';

const { Option } = Select;

@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
class InfoModal extends PureComponent {
  render (){
    const { disCountInfo,modalVisible,changeInfoModalState  } = this.props;
    return (
      <Modal
        title={disCountInfo.name}
        centered
        visible={modalVisible}
        onOk={changeInfoModalState}
        onCancel={changeInfoModalState}
      >
        <div className={styles.modalInfoList}>
          <div>
            <span>优惠券ID：</span>
            <span>{disCountInfo.id}</span>
          </div>
          <div>
            <span>优惠券种类：</span>
            <span>{disCountInfo.category == 1 ? "scratch" : "通用" }</span>
          </div>
          <div>
            <span>优惠券类型：</span>
            <span>{disCountInfo.type == 1 ? "折扣券" : "抵用券" }</span>
          </div>
          <div>
            <span>名称：</span>
            <span>{disCountInfo.name}</span>
          </div>
          <div>
            <span>优惠额度：</span>
              <span>{disCountInfo.amount} {disCountInfo.type == 0 ? "元" : "折"}</span>
          </div>
          <div>
            <span>满减额度：</span>
            <span>{disCountInfo.money} 元</span>
          </div>
          <div>
            <span>有效期：</span>
            <span>{disCountInfo.validityDays} 天</span>
          </div>
        </div>
      </Modal>
    )
  }
}

export default InfoModal;