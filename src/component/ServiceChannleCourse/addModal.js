import React, { Component,PureComponent } from 'react';
import { connect } from 'dva';
import { Modal,Form, Select,Input,message } from 'antd';

const FormItem = Form.Item;
const { Option } = Select;
@Form.create()
@connect(({ service, loading }) => ({
  service,
  loading: loading.models.service,
}))
class AddModal extends PureComponent {
  state= {
    courseType: 0,
  }
  // 新增活动 课程类型选择
  chooseCourseType = e => {
    this.props.chooseCourseTypeFn(e)
  }
  courseChange = (key,data)  => {
    this.props.courseChangeFn(key,data)
  }
  // 检查课程数据
  checkStatus = () => {
    const {form,modalObj} = this.props;
    form.validateFields((err,values) => {
      if(err) {
        return false;
      }
      if(modalObj && modalObj.courseType) {
        this.reWriteCourseSubmit(values);
      } else {
        this.addCourseSubmit(values);
      }
    })
  }
  // 提交课程 
  addCourseSubmit = (values) => {
    const {dispatch,channelId,pageInitFn,cancelFn} = this.props;
    const params = {
      channelId,
      ...values
    }
    dispatch({
      type: 'service/haimawangApiCall',
      payload: {url: 'account/admin/channel/course/add', data : params},
      callback: res =>{
        if(res.result) {
          message.success('新建成功');
          pageInitFn();
          this.cancel();
        } else {
          message.error(res.error);
        }
      }
    })
  }
  // 提交课程 
  reWriteCourseSubmit = (values) => {
    const {dispatch,channelId,pageInitFn,modalObj,cancelFn} = this.props;
    const params = {
      channelId,
      channelCourseId: modalObj.id,
      courseId: modalObj.courseId,
      courseGroupId: modalObj.courseGroupId,
      ...values
    }
    dispatch({
      type: 'service/haimawangApiCall',
      payload: {url: 'account/admin/channel/course/update', data : params},
      callback: res =>{
        if(res.result) {
          message.success('修改成功');
          pageInitFn();
          this.cancel();
        } else {
          message.error(res.error);
        }
      }
    })
  }
  cancel = () => {
    const { cancelFn,form } = this.props;
    cancelFn();
    form.resetFields();
  }
  render (){
    const {form: { getFieldDecorator}, modalObj,modalVisible,cancelFn,courseType,courseList,courseGroupList,courseData } = this.props;
    const formItemLayout = {labelCol: { xs: { span: 24 }, sm: { span: 6 } },wrapperCol: { xs: { span: 24 }, sm: { span: 14 } },};
    const courseTypeList =[ { value: 1, text: '单课程' },{ value: 2, text: '系列课' } ]
    const formField = courseType == 1 ? "courseId" : "courseGroupId";
    return (
      <Modal
        title={modalObj.courseType ? "修改课程" : "新增课程"}
        centered
        visible={modalVisible}
        onOk={this.checkStatus}
        onCancel={this.cancel}
      >
        <Form {...formItemLayout}>
            <FormItem label="课程类型">
              {getFieldDecorator('courseType', {
                initialValue: modalObj.courseType ? "" + modalObj.courseType : "",
                rules: [{ required: true, message: '请选择课程类型' }],
              })(
                <Select placeholder="请选择" onChange= {this.chooseCourseType}>
                  {
                    courseTypeList.map(item => {
                      return <Option key={item.value}>{item.text}</Option>
                    })
                  }
                </Select>
              )}
            </FormItem>
            <FormItem label="课程ID">
              {getFieldDecorator(formField, {
                initialValue: courseType == 1 ? (modalObj.courseId ? modalObj.courseId : "") : (modalObj.courseGroupId ? modalObj.courseGroupId : ""),
                rules: [{ required: true, message: '请选择课程ID' }],
              })(
                <Select
                  placeholder="请选择"
                  style={{ width: '100%' }}
                  onChange={this.courseChange}
                >
                  {courseType == 1 && courseList.map(item => {
                    return <Option key={item.id} val={item}>{item.id}</Option>
                  })}
                  {courseType == 2 && courseGroupList.map(item => {
                    return <Option key={item.id} val={item}>{item.id}</Option>
                  })}
                </Select>
              )}
            </FormItem>
            <FormItem label="课程期次">
              <Input value={modalObj.courseIssue ? modalObj.courseIssue : courseData.issue} disabled style={{background: '#fff',color: 'rgba(0, 0, 0, 0.65)'}}/>
            </FormItem>
            <FormItem label="课程名称">
              <Input value={modalObj.courseName ? modalObj.courseName : courseData.name} disabled style={{background: '#fff',color: 'rgba(0, 0, 0, 0.65)'}}/>
            </FormItem>
            <FormItem label="渠道价格">
              {getFieldDecorator('price', {
                initialValue: modalObj.price && modalObj.price!="" ? modalObj.price : "",
                rules: [{ required: true, message: '' }],
              })(
                <Input />
              )}
            </FormItem>
        </Form>
      </Modal>
    )
  }
}

export default AddModal;